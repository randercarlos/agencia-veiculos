
@extends('backend.template.app')

@section('main_title', 'Autos')

@push('styles')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.4.1/jquery.fancybox.min.css" />

@endpush

@section('content')

    @include('backend.includes.alerts')

    <p class="text-right">
        <a href="{{ route('autos.create') }}" class="btn btn-primary">
            <i class="fa fa-plus-circle"></i> Cadastrar Novo
        </a>
    </p>


    <table class="table table-striped table-bordered" id="tb1">
        <tr>
            <th width="150px" class="text-center">Imagem</th>
            <th width="15%">Marca</th>
            <th width="15%">Modelo</th>
            <th>Versão</th>
            <th width="140px" class="text-center">Em destaque ?</th>
            <th class="text-center" width="140px">
                <i class="fa fa-cog"></i>
            </th>
        </tr>

        @forelse($autos as $auto)
            <tr>
                <td class="text-center">
                    <a href="{{ asset('uploads/auto/' . $auto->photo) }}" data-fancybox="gallery-{{ $auto->auto_id }}"
                     data-caption="{{ $auto->brand_name }} {{ $auto->model_name }} {{ $auto->version_name }}">
                        <img src="{{ asset('uploads/auto/' . $auto->photo) }}" style='width: 100px; height: 70px' />
                    </a>
                </td>
                <td style="vertical-align: middle;"> {{ $auto->brand_name }}</td>
                <td style="vertical-align: middle;"> {{ $auto->model_name }}</td>
                <td style="vertical-align: middle;"> {{ $auto->version_name }}</td>
                <td style="vertical-align: middle;" class="text-center"> {{ $auto->is_featured ? 'Sim' : 'Não' }}</td>
                <td class="text-center" style="vertical-align: middle;">

                    <a href="#" class="btn btn-warning btn-sm" data-toggle="lightbox">
                        <i class="fa fa-photo"></i>
                    </a>

                    <a href="{{ route('autos.edit', $auto->auto_id) }}" class="btn btn-primary btn-sm"
                        data-toggle="tooltip" title="editar">
                        <i class="fa fa-edit"></i>
                    </a>

                    <button data-link="{{ route('autos.destroy', $auto->auto_id ) }}" data-toggle="tooltip"
                        title="remover" class="btn btn-danger btn-sm btn-remover" data-resource="{{ $auto->auto_id }}">
                            <i class="fa fa-trash"></i>
                    </button>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan='6' style='text-align: center;'>Nenhum registro encontrado!</td>
            </tr>
        @endforelse

    </table>


    <div class="row">
        <div class="col-md-12 text-center">
            {{ $autos->links() }}
        </div>
    </div>


    @include('backend.includes.modal_delete', ['title' => 'automóvel', 'text' => 'o automóvel',
        'route' => 'autos.destroy'])


@endsection


@push('scripts')

    <!-- inclui o javascript necessário para exiber o modal para confirmação da exclusão  -->
    <script type="text/javascript" src="{{ asset('assets/js/delete_actions.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.4.1/jquery.fancybox.min.js"></script>

@endpush
