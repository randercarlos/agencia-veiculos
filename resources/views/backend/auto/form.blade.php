
@extends('backend.template.app')

@section('main_title')
    {{ isset($auto) ? 'Editar Auto' : 'Nova Auto' }}
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/libs/summernote/summernote.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <style type="text/css">
        .select2-selection__rendered {
            line-height: 32px !important;
        }

        .select2-selection {
            height: 34px !important;
        }
    </style>
@endpush

@section('content')

    @include('backend.includes.errors')


    @if (isset($auto))
        {!! Form::model($auto, ['route' => ['autos.update', $auto->id], 'method' => 'PUT',
            'files' => true]) !!}
    @else
        {!! Form::open(['route' => 'autos.store', 'files' => true]) !!}
    @endif

    <div class="row">

        <div class="form-group col-md-6 col-xs-12">
            <label for="brand_id">Marca</label>
            {!! Form::select('brand_id', $brands, !empty($auto->brand_id) ? $auto->brand_id : null,
                ['class' => 'form-control', 'style' => 'height: 50px !important;', 'required' => true,
                'placeholder' => 'Selecione uma marca...', 'data-live-search' => 'true']) !!}
        </div>

        <div class="form-group col-md-6 col-xs-12">
            <label for="model_id">Modelo</label>
            {!! Form::select('model_id', $models, !empty($auto->model_id) ? $auto->model_id : null,
                ['class' => 'form-control', 'style' => 'height: 50px !important;', 'required' => true,
                'placeholder' => 'Selecione um modelo...', 'data-live-search' => 'true']) !!}
        </div>

    </div>

    <div class="row">

        <div class="form-group col-md-4 col-xs-12">
            <label for="version_id">Versão</label>
            {!! Form::select('version_id', $versions, !empty($auto->version_id) ? $auto->version_id : null,
                ['class' => 'form-control', 'style' => 'height: 50px !important;', 'required' => true,
                'placeholder' => 'Selecione uma versão...', 'data-live-search' => 'true']) !!}
        </div>

        <div class="form-group col-md-2 col-xs-12">
            <label for="plate_num">Placa</label>
            {!! Form::text('plate_num', null, ['class' => 'form-control altura',
                'placeholder' => 'Informe a placa... ']) !!}
        </div>

        <div class="form-group col-md-2 col-xs-12">
            <label for="plate">Final da Placa</label>
            {!! Form::select('plate', $plates, !empty($auto->plate) ? $auto->plate : null,['data-live-search' => 'true',
                'class' => 'form-control', 'placeholder' => 'Selecione...', 'required' => true]) !!}
        </div>

        <div class="form-group col-md-2 col-xs-12">
            <label for="is_state">Estado do carro</label>
            <select name="is_state" id="is_state" class="form-control" required >
                <option value="">Estado do carro...</option>
                <option value="0">Novo</option>
                <option value="1">Usado</option>
            </select>
        </div>

        <div class="form-group col-md-2 col-xs-12">
            <label for="year">Ano</label>
            {!! Form::select('year', $years, !empty($auto->year) ? $auto->year : null,
                ['class' => 'form-control', 'placeholder' => 'Selecione...', 'data-live-search' => 'true',
                'required' => true]) !!}
        </div>

    </div>

    <div class="row">

        <div class="form-group col-md-2 col-xs-12">
            <label for="gearbox_id">Câmbio</label>
            {!! Form::select('gearbox_id', $gearboxes, !empty($auto->gearbox_id) ? $auto->gearbox_id : null,
                ['class' => 'form-control', 'placeholder' => 'Selecione...', 'data-live-search' => 'true',
                'required' => true]) !!}
        </div>

        <div class="form-group col-md-2 col-xs-12">
            <label for="color_id">Cor</label>
             {!! Form::select('color_id', $colors, !empty($auto->color_id) ? $auto->color_id : null,
                ['class' => 'form-control', 'placeholder' => 'Selecione...', 'data-live-search' => 'true',
                'required' => true]) !!}
        </div>

        <div class="form-group col-md-2 col-xs-12">
            <label for="fuel_id">Combustível</label>
            {!! Form::select('fuel_id', $fuels, !empty($auto->fuel_id) ? $auto->fuel_id : null,
                ['class' => 'form-control', 'placeholder' => 'Selecione...', 'required' => true,
                'data-live-search' => 'true']) !!}
        </div>

        <div class="form-group col-md-2 col-xs-12">
            <label for="bodywork_id">Carroceria</label>
            {!! Form::select('bodywork_id', $bodyworks, !empty($auto->bodywork_id) ? $auto->bodywork_id : null,
                ['class' => 'form-control', 'placeholder' => 'Selecione...', 'required' => true,
                'data-live-search' => 'true']) !!}
        </div>

        <div class="form-group col-md-2 col-xs-12">
            <label for="documentation_id">Documentação</label>
           {!! Form::select('documentation_id', $documentations, !empty($auto->documentation_id) ?
                $auto->documentation_id : null, ['class' => 'form-control', 'required' => true,
                'placeholder' => 'Selecione...', 'data-live-search' => 'true']) !!}
        </div>

        <div class="form-group col-md-2 col-xs-12">
            <label for="need_id">Necessidade</label>
            {!! Form::select('need_id', $needs, !empty($auto->need_id) ? $auto->need_id : null,
                ['class' => 'form-control', 'placeholder' => 'Selecione...', 'required' => true,
                'data-live-search' => 'true']) !!}
        </div>

    </div>

    <div class="row">

        <div class="form-group col-md-2 col-xs-12">
            <label for="ports">Número de portas</label>
            <select name="ports" id="ports" class="form-control" required>
                <option value="">Selecione...</option>
                <option value="0">0</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
            </select>
        </div>

        <div class="form-group col-md-2 col-xs-12">
            <label for="is_negotiable">Tipo de negociação</label>
            {!! Form::select('is_negotiable', $negotiableTypes, !empty($auto->is_negotiable) ?
                $auto->is_negotiable : null, ['class' => 'form-control', 'placeholder' => 'Selecione...',
                'data-live-search' => 'true', 'required' => true]) !!}
        </div>

        <div class="col-md-2 col-xs-12">
            <label for="is_featured" class="required">Em destaque</label>
            <select name="is_featured" id="is_featured" class="form-control" required>
                <option value="">Selecione uma opção...</option>
                <option value="1">SIM</option>
                <option value="0" selected>NÃO</option>
            </select>
        </div>

        <div class="col-md-2 col-xs-12">
            <label for="is_offer required" class="required">Em oferta</label>
            <select name="is_offer" id="is_offer" class="form-control" required>
                <option value="">Selecione uma opção...</option>
                <option value="1">SIM</option>
                <option value="0" selected>NÃO</option>
            </select>
        </div>

        <div class="form-group col-md-2 col-xs-12">
            <label for="km">Kilometragem</label>
            {!! Form::text('km', null, ['class' => 'form-control altura', 'placeholder' => 'Informe a km... ',
            'required' => true]) !!}
        </div>

        <div class="form-group col-md-2 col-xs-12">
            <label for="price">Preço</label>
            {!! Form::text('price', null, ['class' => 'form-control altura money', 'required' => true,
                'placeholder' => 'Informe o preço... ', 'maxlength' => '13']) !!}
        </div>

    </div>

    <div class="row">

        @foreach($optionalsGroup as $group)
            <div class="col-md-12" style="margin-bottom: 10px: height: 100px">
                <h4 style="border-bottom: 1px solid black; font-size: 22px">{{ $group->name }}</h4>

                @foreach($optionals->where('group_id', $group->id) as $optional)
                    <div class="opcionais_grupos">
                        {!! Form::checkbox('auto_optionals[]', $optional->id) !!}
                        <span style="cursor:pointer">{{ $optional->name }}</span>
                    </div>
                @endforeach

            </div>
        @endforeach

    </div>

    <div class="form-group">
        <label for="obs">Depoimento (Opcional)</label>
        {!! Form::textarea('obs', null, []) !!}
    </div>

    <br/>

    <div class="row">

        <div class="form-group col-md-6 col-xs-12">
            <label for="auto_seo_desc">Descrição breve (SEO - Opcional)</label>
            <small class="text-muted pull-right">Meta Description - Até 156 caracteres</small>
            <input type="text" name="auto_seo_desc" id="auto_seo_desc" class="form-control altura"
                placeholder="Informe uma descrição breve do automóvel" />
        </div>

        <div class="form-group col-md-6 col-xs-12">
            <label for="auto_seo_keys">Palavras-chave (SEO - Opcional)</label>
            <small class="text-muted pull-right">Meta Keywords - Até 20 palavras</small>
            <input type="text" name="auto_seo_keys" id="auto_seo_keys" class="form-control altura"
                placeholder="Informe palavras-chave do automóvel" />
        </div>

    </div>

    <div class="row">
        <div class="text-right col-xs-12 col-md-12">
            <div class="hidden-xs">
                <button type="submit" class="btn btn-primary" name="botao_salvar" id="botao_salvar">
                    <i class="fa fa-check-circle"></i> Salvar Dados
                </button>
            </div>
        </div>
    </div>

    {!! Form::close() !!}

@endsection

@push('scripts')

    <script type="text/javascript" src="{{ asset('assets/js/jquery.mask.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.maskMoney.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/libs/summernote/summernote.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            $('input[name="km"]').mask("000.000", {reverse: true});
            $('input[name="plate_num"]').inputmask("AAA-9999");  //static mask

            $('input[name="price"]').maskMoney({showSymbol:true, decimal:",",thousands:".", prefix:"R$ "});

            $('textarea[name="obs"]').summernote({
                height: 200
            });

            // remove a máscara ao submeter o form
            $('#botao_salvar').click(function() {
                $('input[name="km"]').maskMoney('unmasked');
                $('input[name="price"]').unmask();
            });

            // Aplica o select2 nos dropdowns...
            $("select[name='brand_id'], select[name='model_id'], select[name='version_id'], " +
                "select[name='plate_num'], select[name='plate'], select[name='is_state'], " +
                "select[name='year'], select[name='gearbox_id'], select[name='color_id'], " +
                "select[name='bodywork_id'], select[name='fuel_id'], select[name='color_id'], " +
                "select[name='documentation_id'], select[name='need_id'], select[name='ports'], " +
                "select[name='is_negotiable'], select[name='is_featured'], select[name='is_offer'] ").select2({
                    dropdownAutoWidth: true
            });

            $("select[name='brand_id']").change(function() {
            	var default_option_model = $("select[name='model_id'] option:first").clone(true);
            	var default_option_version = $("select[name='version_id'] option:first").clone(true);

        	    $("select[name='model_id'], select[name='version_id']").empty()
                    .append('<option value="">Carregando...</option>');

        	    var model_id = $(this).val();
                var url_models = "{{ env('APP_URL') }}admin/brands/" + model_id + "/models";

	    	    // Query the responses for this demographic:
	    	    $.get(url_models, { model_id: model_id }, function(data){

	    	    	$("select[name='model_id']").empty().append(default_option_model);
	    	    	$("select[name='version_id']").empty().append(default_option_version);

                    // Loop over the new response json and populate the select list:
                    $.each( data, function( index, item ) {
                        $("select[name='model_id']").append( "<option value='" + item.id + "'>" + item.name +
                            "</option>");
                    });

	    	    });

            });


            $("select[name='model_id']").change(function() {
            	var default_option_version = $("select[name='version_id'] option:first").clone(true);

            	$("select[name='version_id']").empty().append('<option value="">Carregando...</option>');

        	    var model_id = $(this).val();
                var url_versions = "{{ env('APP_URL') }}admin/models/" + model_id + "/versions";

	    	    // Query the responses for this demographic:
	    	    $.get(url_versions, { model_id: model_id }, function(data){

	    	    	$("select[name='version_id']").empty().append(default_option_version);

                    // Loop over the new response json and populate the select list:
                    $.each( data, function( index, item ) {
                        $("select[name='version_id']").append( "<option value='" + item.id + "'>" + item.name +
                            "</option>");
                    });

	    	    });

            });

        });
     </script>

@endpush
