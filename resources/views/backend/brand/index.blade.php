
@extends('backend.template.app')

@section('main_title', 'Marcas')

@section('content')
    
    
    @include('backend.includes.alerts')
    
    
    <p class="text-right">
        <a href="{{ route('brands.create') }}" class="btn btn-primary">
            <i class="fa fa-plus-circle"></i> Cadastrar Novo
        </a>
    </p>
    
    
    <table class="table table-striped table-bordered" id="tb1">
        <tr>
            <th width="11%" class="text-center">Imagem</th>
            <th width="25%">Nome</th>
            <th>URL</th>
            <th class="text-center" width="10%">
                <i class="fa fa-cog"></i>
            </th>
        </tr>
        
        @forelse($brands as $brand)        
            <tr>
                <td class="text-center">
                    <img src="{{ asset('uploads/brand/' . $brand->photo) }}" style='width: 100px; height: 70px' />
                </td>
                <td style="vertical-align: middle;"> {{ $brand->name }}</td>
                <td style="vertical-align: middle;"> {{ $brand->url }}</td>
                <td class="text-center" style="vertical-align: middle;">
                 
                    <a href="{{ route('brands.edit', $brand->id) }}" class="btn btn-primary btn-sm" 
                        data-toggle="tooltip" title="editar">
                        <i class="fa fa-edit"></i>
                    </a>
                    
                    <button data-link="{{ route('brands.destroy', $brand->id) }}" data-toggle="tooltip" 
                        title="remover" class="btn btn-danger btn-sm btn-remover" data-resource="{{ $brand->name }}">
                            <i class="fa fa-trash"></i> 
                    </button>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan='4' style='text-align: center;'>Nenhum registro encontrado!</td>            
            </tr>
        @endforelse
        
    </table>

    
    <div class="row">
        <div class="col-md-12 text-center">
            {{ $brands->links() }}        
        </div>
    </div>  
    
    
    @include('backend.includes.modal_delete', ['title' => 'marca', 'text' => 'a marca', 'route' => 'brands.destroy'])
    
    
@endsection


@push('scripts')
    
    <!-- inclui o javascript necessário para exiber o modal para confirmação da exclusão  -->  
    <script type="text/javascript" src="{{ asset('assets/js/delete_actions.js') }}"></script>
     
@endpush