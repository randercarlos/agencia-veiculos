
@extends('backend.template.app')

@section('main_title')
    {{ isset($brand) ? 'Editar Marca' : 'Nova Marca' }}
@endsection


@section('content')
    
    @include('backend.includes.errors')
    
    
    @if (isset($brand))
        {!! Form::model($brand, ['route' => ['brands.update', $brand->id], 'method' => 'PUT', 
            'files' => true]) !!}
    @else
        {!! Form::open(['route' => 'brands.store', 'files' => true]) !!}
    @endif
    
    <div class="row">
        <div class="form-group col-md-5">
            <label for="name">Nome</label>
            {!! Form::text('name', null, ['class' => 'form-control altura', 
                    'placeholder' => 'Informe o nome do marca']) !!}
        </div>     
        
        <div class="form-group col-md-7">
            <label for="url">Url</label>
            {!! Form::text('url', null, ['class' => 'form-control altura', 
                'placeholder' => 'http://www.sitedamarca.com.br ']) !!}
        </div>        
    </div>
    
    <br/><br/>
      
    <div class="form-group row">   
        <div class="col-md-10">   
            <span class="btn btn-black btn-file col-xs-12 col-md-4 ">
                <i class="fa fa-cloud-upload"></i>  
                Selecione um Foto {!! Form::file('photo') !!}
            </span>
            
            &nbsp; &nbsp; &nbsp;
             
            <span class="text-muted" style="line-height: 50px">Dimensões:  até 150 Largura x 100 Altura (pixels)</span>
        </div>
        
        <div class="col-md-2">
            @if (isset($brand) && !empty($brand->photo))                            
                <p class="text-right">
                    <img class="img-thumbnail" style="height:80px; "
                         src="{{ asset('uploads/brand/' . $brand->photo) }}" />
                </p>    
            @endif
        </div>
        
    </div>
    
    
    <div class="row">    
        <div class="text-right col-xs-12">
            <div class="hidden-xs">
                <button type="submit" class="btn btn-primary"><i class="fa fa-refresh"></i> Salvar Dados</button>
            </div>
        </div>
    </div>
    
        
    {!! Form::close() !!}    

@endsection

