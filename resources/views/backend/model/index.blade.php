
@extends('backend.template.app')

@section('main_title', 'Modelos')

@section('content')
    
    @include('backend.includes.alerts')
   
    <p class="text-right">
        <a href="{{ route('models.create') }}" class="btn btn-primary">
            <i class="fa fa-plus-circle"></i> Cadastrar Novo
        </a>
    </p>
    
    
    <table class="table table-striped table-bordered" id="tb1">
        <tr>
            <th>Nome</th>
            <th width="25%">Marca</th>
            <th width="11%" class="text-center">Imagem</th>
            <th class="text-center" width="10%">
                <i class="fa fa-cog"></i>
            </th>
        </tr>
        
        @forelse($models as $model)        
            <tr>
                <td style="vertical-align: middle;"> {{ $model->name }}</td>
                <td style="vertical-align: middle;"> {{ $model->brand->name }} </td>
                <td class="text-center">
                    <img src="{{ asset('uploads/brand/' . $model->brand->photo) }}" 
                        style='width: 100px; height: 70px' />
                </td>
                <td class="text-center" style="vertical-align: middle;">
                 
                    <a href="{{ route('models.edit', $model->id) }}" class="btn btn-primary btn-sm" 
                        data-toggle="tooltip" title="editar">
                        <i class="fa fa-edit"></i>
                    </a>
                    
                    <button data-link="{{ route('models.destroy', $model->id) }}" data-toggle="tooltip" 
                        title="remover" class="btn btn-danger btn-sm btn-remover" data-resource="{{ $model->name }}">
                            <i class="fa fa-trash"></i> 
                    </button>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan='4' style='text-align: center;'>Nenhum registro encontrado!</td>            
            </tr>
        @endforelse
        
    </table>
    
      
    <div class="row">
        <div class="col-md-12 text-center">
            {{ $models->links() }}        
        </div>
    </div>  
    
    
    @include('backend.includes.modal_delete', ['title' => 'modelo', 'text' => 'o modelo', 'route' => 'models.destroy'])
    
    
@endsection


@push('scripts')
    
    <!-- inclui o javascript necessário para exiber o modal para confirmação da exclusão  -->  
    <script type="text/javascript" src="{{ asset('assets/js/delete_actions.js') }}"></script>
     
@endpush