
@extends('backend.template.app')

@section('main_title')
    {{ isset($model) ? 'Editar Modelo' : 'Novo Modelo' }}
@endsection

@push('styles')
    <link rel="stylesheet" 
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
@endpush


@section('content')
    
    @include('backend.includes.errors')
    
    
    @if (isset($model))
        {!! Form::model($model, ['route' => ['models.update', $model->id], 'method' => 'PUT', 
            'files' => true]) !!}
    @else
        {!! Form::open(['route' => 'models.store', 'files' => true]) !!}
    @endif
    
   
    <div class="row">
        <div class="form-group col-md-6">
            <label for="url">Marca</label>
            {!! Form::select('brand_id', $brands, !empty($model->brand_id) ? $model->brand_id : null, 
                ['class' => 'form-control selectpicker show-tick', 
                'placeholder' => 'Selecione uma marca...', 'data-live-search' => 'true'], 
                ['data-thumbnail' => 'teste.jpg' ]) !!}
        </div>
                
        <div class="form-group col-md-6">
            <label for="name">Nome</label>
            {!! Form::text('name', null, ['class' => 'form-control altura', 
                    'placeholder' => 'Informe o nome do modelo...']) !!}
        </div>     
    </div>
    
    <br/><br/>
    
    <div class="row">    
        <div class="text-right col-xs-12">
            <div class="hidden-xs">
                <button type="submit" class="btn btn-primary"><i class="fa fa-refresh"></i> Salvar Dados</button>
            </div>
        </div>
    </div>
    
        
    {!! Form::close() !!}    

@endsection


@push('scripts')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function(){
        	$("select[name='brand_id']").selectpicker({
                style: null    
            });
        });
        
     </script>
     
@endpush
