
@extends('backend.template.app')

@section('main_title', 'Serviços')

@section('content')
    
    
    @include('backend.includes.alerts')
    
    
    <p class="text-right">
        <a href="{{ route('services.create') }}" class="btn btn-primary">
            <i class="fa fa-plus-circle"></i> Cadastrar Novo
        </a>
    </p>
    
    
    <table class="table table-striped table-bordered" id="tb1">
        <tr>
            <th width="7%">Foto</th>
            <th width="28%">Nome</th>
            <th width="55%">Descrição</th>
            <th class="text-center" width="10%">
                <i class="fa fa-cog"></i>
            </th>
        </tr>
        
        @forelse($services as $service)        
            <tr>
                <td>
                    <img src="{{ asset('uploads/service/' . $service->photo) }}" style='width: 60px; height: 60px' />
                </td>
                <td style="vertical-align: middle;"> {{ $service->name }}</td>
                <td> {{ $service->description }}</td>
                <td class="text-center">
                 
                    <a href="{{ route('services.edit', $service->id) }}" class="btn btn-primary btn-sm" 
                        data-toggle="tooltip" title="editar">
                        <i class="fa fa-edit"></i>
                    </a>
                    
                    <button data-link="{{ route('services.destroy', $service->id) }}" data-toggle="tooltip" 
                        title="remover" class="btn btn-danger btn-sm btn-remover" data-resource="{{ $service->name }}">
                            <i class="fa fa-trash"></i> 
                    </button>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan='4' style='text-align: center;'>Nenhum registro encontrado!</td>            
            </tr>
        @endforelse
        
    </table>
    
    <div class="row">
        <div class="col-md-12 text-center">
            {{ $services->links() }}        
        </div>
    </div>
    
    @include('backend.includes.modal_delete', ['title' => 'serviço', 'text' => 'o serviço', 
        'route' => 'services.destroy'])
    
    
@endsection


@push('scripts')
    
    <!-- inclui o javascript necessário para exiber o modal para confirmação da exclusão  -->  
    <script type="text/javascript" src="{{ asset('assets/js/delete_actions.js') }}"></script>
     
@endpush