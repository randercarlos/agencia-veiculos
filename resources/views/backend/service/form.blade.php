
@extends('backend.template.app')

@section('main_title')
    {{ isset($service) ? 'Editar serviço' : 'Novo serviço' }}
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/libs/summernote/summernote.css') }}">
@endpush

@section('content')
    
    @include('backend.includes.errors')
    
    
    @if (isset($service))
        {!! Form::model($service, ['route' => ['services.update', $service->id], 'method' => 'PUT', 
            'files' => true]) !!}
    @else
        {!! Form::open(['route' => 'services.store', 'files' => true]) !!}
    @endif
    
    <div class="row">
        <div class="form-group col-md-6">
            <label for="name">Nome</label>
            {!! Form::text('name', null, ['class' => 'form-control altura', 
                    'placeholder' => 'Informe o nome do serviço']) !!}
        </div>             
    </div>
    
    <br/><br/>
      
    <div class="form-group row">   
        <div class="col-md-10">   
            <span class="btn btn-black btn-file col-xs-12 col-md-4 ">
                <i class="fa fa-cloud-upload"></i>  
                Selecione um Foto {!! Form::file('photo') !!}
            </span>
            
            &nbsp; &nbsp; &nbsp;
             
            <span class="text-muted" style="line-height: 50px">Dimensões:  60 Largura x 60 Altura (pixels)</span>
        </div>
        
        <div class="col-md-2">
            @if (isset($service) && !empty($service->photo))                            
                <p class="text-right">
                    <img class="img-thumbnail" style="height:80px; "
                         src="{{ asset('uploads/service/' . $service->photo) }}" />
                </p>    
            @endif
        </div>
        
    </div>
    
    <div class="form-group">
        <label for="serviço_desc">serviço</label>
         {!! Form::textarea('description', null, ['placeholder' => 'Informe a descrição do serviço...']) !!}
    </div>
    
    <div class="row">    
        <div class="text-right col-xs-12">
            <div class="hidden-xs">
                <button type="submit" class="btn btn-primary"><i class="fa fa-refresh"></i> Salvar Dados</button>
            </div>
        </div>
    </div>
    
        
    {!! Form::close() !!}    

@endsection

@push('scripts')

    <script type="text/javascript" src="{{ asset('assets/libs/summernote/summernote.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('textarea[name="description"]').summernote({
                height: 200
            });
        });
     </script>
     
@endpush