
@extends('backend.template.app')

@section('main_title', 'Câmbios')

@section('content')
    
    @include('backend.includes.errors')
    
    
    @if (isset($gearbox))
        {!! Form::model($gearbox, ['route' => ['gearboxes.update', $gearbox->id], 'method' => 'PUT']) !!}
    @else
        {!! Form::open(['route' => 'gearboxes.store']) !!}
    @endif
    

    <div class="row">
    
        <div class="col-md-10">
            <label for="name">Nome:</label>
            {!! Form::text('name', null, ['class' => 'form-control altura', 
                'placeholder' => 'Informe o nome do câmbio...']) !!}
        </div>
                 
        <div class="col-md-2">
            <br />
            
            <button type="submit" class="btn btn-primary"><i class="fa fa-refresh"></i> Salvar Dados</button>
        </div>
                   
    </div>
    
    
    {!! Form::close() !!}    

@endsection

