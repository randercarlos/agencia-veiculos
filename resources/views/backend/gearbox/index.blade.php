
@extends('backend.template.app')

@section('main_title', 'Câmbios')

@section('content')
    
    
    @include('backend.includes.alerts')
    
    
    <p class="text-right">
        <a href="{{ route('gearboxes.create') }}" class="btn btn-primary">
            <i class="fa fa-plus-circle"></i> Cadastrar Novo
        </a>
    </p>
    
    
    <table class="table table-striped table-bordered" id="tb1">
        <tr>
            <th width="90%">Nome</th>
            <th class="text-center" width="10%">
                <i class="fa fa-cog"></i>
            </th>
        </tr>
        
        @forelse($gearboxes as $gearbox)        
            <tr>
                <td> {{ $gearbox->name }}</td>
                <td class="text-center">
                 
                    <a href="{{ route('gearboxes.edit', $gearbox->id) }}" class="btn btn-primary btn-sm" 
                        data-toggle="tooltip" title="editar">
                        <i class="fa fa-edit"></i>
                    </a>
                    
                    <button data-link="{{ route('gearboxes.destroy', $gearbox->id) }}" data-resource="{{ $gearbox->name }}"
                            class="btn btn-danger btn-sm btn-remover" data-toggle="tooltip" title="remover">
                            <i class="fa fa-trash"></i> 
                    </button>
                </td>
            </tr>
        @empty
        
            <tr>
                <td colspan='2' style='text-align: center;'>Nenhum registro encontrado!</td>            
            </tr>
            
        @endforelse
    </table>
    
    
    <div class="row">
        <div class="col-md-12 text-center">
            {{ $gearboxes->links() }}        
        </div>
    </div>
    
    @include('backend.includes.modal_delete', ['title' => 'câmbio', 'text' => 'o câmbio', 
        'route' => 'gearboxes.destroy'])
    
    
@endsection


@push('scripts')
    
    <!-- inclui o javascript necessário para exiber o modal para confirmação da exclusão  -->  
    <script type="text/javascript" src="{{ asset('assets/js/delete_actions.js') }}"></script>
     
@endpush