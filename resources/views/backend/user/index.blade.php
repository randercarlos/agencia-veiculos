
@extends('backend.template.app')

@section('main_title', 'Usuários')

@section('content')
    
    
    @include('backend.includes.alerts')
    
    
    <p class="text-right">
        <a href="{{ route('users.create') }}" class="btn btn-primary">
            <i class="fa fa-plus-circle"></i> Cadastrar Novo
        </a>
    </p>
    
    
    <table class="table table-striped table-bordered" id="tb1">
        <tr>
            <th width="60%">Nome</th>
            <th width="30%">Email</th>
            <th class="text-center" width="10%">
                <i class="fa fa-cog"></i>
            </th>
        </tr>
        
        @forelse($users as $user)
            <tr>
                <td> {{ $user->name }}</td>
                <td> {{ $user->email }}</td>
                <td class="text-center">
                 
                    <a href="{{ route('users.edit', $user->id) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" 
                        title="editar">
                        <i class="fa fa-edit"></i>
                    </a>
                    
                    <button data-link="{{ route('users.destroy', $user->id) }}" data-resource="{{ $user->name }}"
                            class="btn btn-danger btn-sm btn-remover" data-toggle="tooltip" title="remover">
                            <i class="fa fa-trash"></i> 
                    </button>
                </td>
            </tr>
        @empty
        
            <tr>
                <td colspan='3' style='text-align: center;'>Nenhum registro encontrado!</td>            
            </tr>
            
        @endforelse
    </table>
    
    
    <div class="row">
        <div class="col-md-12 text-center">
            {{ $users->links() }}        
        </div>
    </div>
    
    
    @include('backend.includes.modal_delete', ['title' => 'usuário', 'text' => 'o usuário', 
        'route' => 'users.destroy'])
        
    
@endsection


@push('scripts')
    
    <!-- inclui o javascript necessário para exiber o modal para confirmação da exclusão  -->  
    <script type="text/javascript" src="{{ asset('assets/js/delete_actions.js') }}"></script>
     
@endpush