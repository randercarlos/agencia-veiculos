
@extends('backend.template.app')

@section('main_title', 'Usuários')

@section('content')
    
    @include('backend.includes.errors')
    
    
    @if (isset($user))
        {!! Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'PUT']) !!}
    @else
        {!! Form::open(['route' => 'users.store']) !!}
    @endif
    
    
    <div class="row">
    
        <div class="form-group col-md-8">
            <label for="name">Nome Completo:</label>
            {!! Form::text('name', null, ['class' => 'form-control altura', 
                    'placeholder' => 'Informe o nome completo do usuário']) !!}
        </div>
                 
        <div class="form-group col-md-4">
            <label for="email">Email:</label>
            {!! Form::email('email', null, ['class' => 'form-control altura', 
                'placeholder' => 'Informe o email']) !!}
        </div>
                   
    </div>
    
    <div class="row">
        
        <div class="form-group col-md-6">
            <label for="password">Senha:</label>
            {!! Form::password('password',['class' => 'form-control altura', 
                    'placeholder' => 'Informe a senha(mínimo de 5 caracteres)']) !!}
        </div>
        
        <div class="form-group col-md-6">
            <label for="password_confirm">Confirmar senha:</label>
            {!! Form::password('password_confirm', ['class' => 'form-control altura', 
                   'placeholder' => 'Confirme a senha...']) !!}
        </div>
    
    </div>
    
    <div class="row">    
        <div class="text-right col-xs-12">
            <div class="hidden-xs">
                <button type="submit" class="btn btn-primary"><i class="fa fa-refresh"></i> Salvar Dados</button>
            </div>
        </div>
    </div>
        
    {!! Form::close() !!}    

@endsection

