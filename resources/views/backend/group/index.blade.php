
@extends('backend.template.app')

@section('main_title', 'Grupos de Opcionais')

@section('content')
    
    
    @include('backend.includes.alerts')
    
    
    <p class="text-right">
        <a href="{{ route('groups.create') }}" class="btn btn-primary">
            <i class="fa fa-plus-circle"></i> Cadastrar Novo
        </a>
    </p>
    
    
    <table class="table table-striped table-bordered" id="tb1">
        <tr>
            <th>Nome</th>
            <th class="text-center" width="150px">
                <i class="fa fa-cog"></i>
            </th>
        </tr>
        
        @forelse($groups as $group)        
            <tr>
                <td> {{ $group->name }}</td>
                <td class="text-center">
                
                    <a href="{{ route('groups.optionals', $group->id) }}" class="btn btn-info btn-sm" 
                        data-toggle="tooltip" title="Itens do grupo">
                        <i class="fa fa-tag"></i>
                    </a>
                 
                    <a href="{{ route('groups.edit', $group->id) }}" class="btn btn-primary btn-sm" 
                        data-toggle="tooltip" title="editar">
                        <i class="fa fa-edit"></i>
                    </a>
                    
                    <button data-link="{{ route('groups.destroy', $group->id) }}" data-resource="{{ $group->name }}"
                            class="btn btn-danger btn-sm btn-remover" data-toggle="tooltip" title="remover">
                            <i class="fa fa-trash"></i> 
                    </button>
                </td>
            </tr>
        @empty
        
            <tr>
                <td colspan='2' style='text-align: center;'>Nenhum registro encontrado!</td>            
            </tr>
            
        @endforelse
    </table>
    
     
    <div class="row">
        <div class="col-md-12 text-center">
            {{ $groups->links() }}        
        </div>
    </div>
    
    
    @include('backend.includes.modal_delete', ['title' => 'grupo', 'text' => 'o grupo', 'route' => 'groups.destroy'])
    
    
@endsection


@push('scripts')
    
    <!-- inclui o javascript necessário para exiber o modal para confirmação da exclusão  -->  
    <script type="text/javascript" src="{{ asset('assets/js/delete_actions.js') }}"></script>
     
@endpush