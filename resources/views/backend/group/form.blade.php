
@extends('backend.template.app')

@section('main_title', 'Grupos')

@section('content')
    
    @include('backend.includes.errors')
    
    
    @if (isset($group))
        {!! Form::model($group, ['route' => ['groups.update', $group->id], 'method' => 'PUT']) !!}
    @else
        {!! Form::open(['route' => 'groups.store']) !!}
    @endif
    

    <div class="row">
    
        <div class="col-md-12">
            <label for="name">Nome:</label>
            {!! Form::text('name', null, ['class' => 'form-control altura', 
                'placeholder' => 'Informe o nome do grupo...']) !!}
        </div>
                   
    </div>
    
    <br/><br/>
    
    <div class="row">    
        <div class="text-right col-xs-12">
            <div class="hidden-xs">
                <a href="{{ route('groups.index') }}" class="btn btn-warning"> Voltar a Listagem</a>
                <button type="submit" class="btn btn-primary"><i class="fa fa-refresh"></i> Salvar Dados</button>
            </div>
        </div>
    </div>
    
    
    {!! Form::close() !!}    

@endsection

