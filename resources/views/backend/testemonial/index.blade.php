
@extends('backend.template.app')

@section('main_title', 'Depoimentos')

@section('content')
    
    
    @include('backend.includes.alerts')
    
    
    <p class="text-right">
        <a href="{{ route('testemonials.create') }}" class="btn btn-primary">
            <i class="fa fa-plus-circle"></i> Cadastrar Novo
        </a>
    </p>
    
    
    <table class="table table-striped table-bordered" id="tb1">
        <tr>
            <th width="7%">Foto</th>
            <th width="28%">Nome</th>
            <th width="55%">Depoimento</th>
            <th class="text-center" width="10%">
                <i class="fa fa-cog"></i>
            </th>
        </tr>
        
        @forelse($testemonials as $testemonial)        
            <tr>
                <td>
                    <img src="{{ asset('uploads/testemonial/' . $testemonial->photo) }}" />
                </td>
                <td style="vertical-align: middle;"> {{ $testemonial->name }}</td>
                <td> {{ $testemonial->description }}</td>
                <td class="text-center">
                 
                    <a href="{{ route('testemonials.edit', $testemonial->id) }}" class="btn btn-primary btn-sm" 
                        data-toggle="tooltip" title="editar">
                        <i class="fa fa-edit"></i>
                    </a>
                    
                    <button data-link="{{ route('testemonials.destroy', $testemonial->id) }}" 
                            class="btn btn-danger btn-sm btn-remover" data-toggle="tooltip" title="remover"
                            data-resource="{{ $testemonial->name }}">
                            <i class="fa fa-trash"></i> 
                    </button>
                </td>
            </tr>
        @empty
        
            <tr>
                <td colspan='4' style='text-align: center;'>Nenhum registro encontrado!</td>            
            </tr>
            
        @endforelse
    </table>
    
    <div class="row">
        <div class="col-md-12 text-center">
            {{ $testemonials->links() }}        
        </div>
    </div>
    
    @include('backend.includes.modal_delete', ['title' => 'depoimento', 'text' => 'o depoimento', 
        'route' => 'testemonials.destroy'])
    
    
@endsection


@push('scripts')
    
    <!-- inclui o javascript necessário para exiber o modal para confirmação da exclusão  -->  
    <script type="text/javascript" src="{{ asset('assets/js/delete_actions.js') }}"></script>
     
@endpush