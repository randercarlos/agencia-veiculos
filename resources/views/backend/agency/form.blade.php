
@extends('backend.template.app')

@section('main_title', 'Agência')

@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/libs/summernote/summernote.css') }}">
@endpush

@section('content')

    {!! Form::model($agency, ['route' => ['agency.update'], 'method' => 'PUT', 'files' => true]) !!}

    {!! csrf_field() !!}
    
    @include('backend.includes.alerts')
    @include('backend.includes.errors')
    
    
    <div class="row">
    
        <div class="form-group col-md-3">
            <label for="name">Nome:</label>
            {!! Form::text('name', null, ['class' => 'form-control altura', 
                'placeholder' => 'Informe o nome da agência']) !!}
        </div>
                 
        <div class="form-group col-md-3">
            <label for="phone">Telefone(WhatsApp):</label>
            {!! Form::text('phone', null, ['class' => 'form-control altura', 
                'placeholder' => 'Informe o telefone']) !!}
        </div>
                   
        <div class="form-group col-md-3">
            <label for="phone2">Telefone 2:</label>
            {!! Form::text('phone2', null, ['class' => 'form-control altura', 
                'placeholder' => 'Informe o telefone']) !!}
        </div>
                
        <div class="form-group col-md-3">
            <label for="phone3">Telefone 3:</label>
            {!! Form::text('phone3', null, ['class' => 'form-control altura', 
                'placeholder' => 'Informe o telefone']) !!}
        </div>
              
        <div class="form-group col-md-9">
            <label for="address">Endereço completo da agência:</label>
             {!! Form::text('address', null, ['class' => 'form-control altura', 
                'placeholder' => 'Informe o endereço']) !!}
        </div>
                                 
        <div class="form-group col-md-3">
            <label for="business_hours">Horário de funcionamento:</label>
           {!! Form::text('business_hours', null, ['class' => 'form-control altura', 
                'placeholder' => 'Informe os horários de funcionamento']) !!}
        </div>                         
    </div>
    
    <br/><br/>
    
    <div class="form-group row">   
        <div class="col-md-10">   
            <span class="btn btn-black btn-file col-xs-12 col-md-4 ">
                <i class="fa fa-cloud-upload"></i>  
                Logo do Site {!! Form::file('logotipo') !!}
            </span>
            &nbsp; &nbsp; &nbsp; 
            <span class="text-muted" style="line-height: 50px">Dimensões: 155 Largura x 40 Altura (pixels)</span>
        </div>
        
        <div class="col-md-2">                            
            @if (isset($agency->photo))
                <p class="text-right">
                    <img class="img-thumbnail" style="height:80px;" alt="{{ $agency->id }}"
                         src="{{ asset('uploads/agency/' . $agency->photo) }}" />
                </p>    
            @endif
        </div>
    </div>
    
    
    <div class="form-group">
        {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Título no site']) !!}
        {!! Form::textarea('description', null) !!}
    </div>
    
    
    <div class="row">    
        <div class="form-group col-md-6 col-xs-12">
            <label for="seo_description">Descrição breve (SEO - Opcional):</label>
            <small class="text-muted pull-right">Meta Description - Até 156 caracteres</small>
             {!! Form::text('seo_description', null, ['class' => 'form-control altura', 
                'placeholder' => 'Informe uma descrição breve da agência']) !!}
        </div>
        
        <div class="form-group col-md-6 col-xs-12">
            <label for="seo_keys">Palavras-chave (SEO - Opcional):</label>
            <small class="text-muted pull-right">Meta Keywords - Até 20 palavras</small>
             {!! Form::text('seo_keys', null, ['class' => 'form-control altura', 
                'placeholder' => 'Informe palavras-chave da agência']) !!}
        </div>                        
    </div>
    
                     
    <div class="row">    
        <div class="text-right col-xs-12">
            <div class="hidden-xs">
                <button type="submit" class="btn btn-primary"><i class="fa fa-refresh"></i> Atualizar Dados</button>
            </div>
        </div>
    </div>
        
    {!! Form::close() !!}  
    
@endsection

@push('scripts')

    <script type="text/javascript" src="{{ asset('assets/libs/summernote/summernote.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
        	$('textarea[name="description"]').summernote({
                height: 200
            });
        });
     </script>
     
@endpush