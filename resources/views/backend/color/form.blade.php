
@extends('backend.template.app')

@section('main_title', 'Cores')

@section('content')
    
    @include('backend.includes.errors')
    
    
    @if (isset($color))
        {!! Form::model($color, ['route' => ['colors.update', $color->id], 'method' => 'PUT']) !!}
    @else
        {!! Form::open(['route' => 'colors.store']) !!}
    @endif
    

    <div class="row">
    
        <div class="col-md-10">
            <label for="name">Nome:</label>
            {!! Form::text('name', null, ['class' => 'form-control altura', 
                'placeholder' => 'Informe o nome da cor...']) !!}
        </div>
                 
        <div class="col-md-2">
            <br />
            
            <button type="submit" class="btn btn-primary"><i class="fa fa-refresh"></i> Salvar Dados</button>
        </div>
                   
    </div>
    
    
    {!! Form::close() !!}    

@endsection

