
@extends('backend.template.app')

@section('main_title', 'Cores')

@section('content')
    
    
    @include('backend.includes.alerts')
    
    
    <p class="text-right">
        <a href="{{ route('colors.create') }}" class="btn btn-primary">
            <i class="fa fa-plus-circle"></i> Cadastrar Novo
        </a>
    </p>
    
    
    <table class="table table-striped table-bordered" id="tb1">
        <tr>
            <th width="90%">Nome</th>
            <th class="text-center" width="10%">
                <i class="fa fa-cog"></i>
            </th>
        </tr>
        
        @forelse($colors as $color)        
            <tr>
                <td> {{ $color->name }}</td>
                <td class="text-center">
                 
                    <a href="{{ route('colors.edit', $color->id) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" 
                        title="editar">
                        <i class="fa fa-edit"></i>
                    </a>
                    
                    <button data-link="{{ route('colors.destroy', $color->id) }}" data-resource="{{ $color->name }}"
                            class="btn btn-danger btn-sm btn-remover" data-toggle="tooltip" title="remover">
                            <i class="fa fa-trash"></i> 
                    </button>
                </td>
            </tr>
        @empty
        
            <tr>
                <td colspan='2' style='text-align: center;'>Nenhum registro encontrado!</td>            
            </tr>
            
        @endforelse
    </table>
    
    
    <div class="row">
        <div class="col-md-12 text-center">
            {{ $colors->links() }}        
        </div>
    </div>
    
    
    @include('backend.includes.modal_delete', ['title' => 'cor', 'text' => 'a cor', 'route' => 'colors.destroy'])
    
    
@endsection


@push('scripts')
    
    <!-- inclui o javascript necessário para exiber o modal para confirmação da exclusão  -->  
    <script type="text/javascript" src="{{ asset('assets/js/delete_actions.js') }}"></script>
     
@endpush