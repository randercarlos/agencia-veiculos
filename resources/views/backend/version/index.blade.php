
@extends('backend.template.app')

@section('main_title', 'Versões')

@section('content')
    
    @include('backend.includes.alerts')
   
    <p class="text-right">
        <a href="{{ route('versions.create') }}" class="btn btn-primary">
            <i class="fa fa-plus-circle"></i> Cadastrar Novo
        </a>
    </p>
    
    
    <table class="table table-striped table-bordered" id="tb1">
        <tr>
            <th width="11%" class="text-center">Imagem</th>
            <th width="25%">Marca</th>
            <th width="25%">Modelo</th>
            <th>Versão</th>
            <th class="text-center" width="10%">
                <i class="fa fa-cog"></i>
            </th>
        </tr>
        
        @forelse($versions as $version)        
            <tr>
                <td class="text-center">
                    <img src="{{ asset('uploads/brand/' . $version->model->brand->photo) }}" 
                        style='width: 100px; height: 70px' />
                </td>
                <td style="vertical-align: middle;"> {{ $version->model->brand->name }} </td>
                <td style="vertical-align: middle;"> {{ $version->model->name }} </td>
                <td style="vertical-align: middle;"> {{ $version->name }}</td>
                <td class="text-center" style="vertical-align: middle;">
                 
                    <a href="{{ route('versions.edit', $version->id) }}" class="btn btn-primary btn-sm" 
                        data-toggle="tooltip" title="editar">
                        <i class="fa fa-edit"></i>
                    </a>
                    
                    <button data-link="{{ route('versions.destroy', $version->id) }}" data-toggle="tooltip" 
                        title="remover" class="btn btn-danger btn-sm btn-remover" data-resource="{{ $version->name }}">
                            <i class="fa fa-trash"></i> 
                    </button>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan='5' style='text-align: center;'>Nenhum registro encontrado!</td>            
            </tr>
        @endforelse
        
    </table>
    
    
    <div class="row">
        <div class="col-md-12 text-center">
            {{ $versions->links() }}        
        </div>
    </div>
      
    
    @include('backend.includes.modal_delete', ['title' => 'versão', 'text' => 'a versão', 
        'route' => 'versions.destroy'])
    
    
@endsection


@push('scripts')
    
    <!-- inclui o javascript necessário para exibir o modal para confirmação da exclusão  -->  
    <script type="text/javascript" src="{{ asset('assets/js/delete_actions.js') }}"></script>
     
@endpush