
@extends('backend.template.app')

@section('main_title')
    {{ isset($version) ? 'Editar Modelo' : 'Novo Modelo' }}
@endsection

@push('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <style type="text/css">
        .select2-selection__rendered {
            line-height: 32px !important;
        }
        
        .select2-selection {
            height: 34px !important;
        }
    </style>
@endpush


@section('content')
    
    @include('backend.includes.errors')
    
    
    @if (isset($version))
        {!! Form::model($version, ['route' => ['versions.update', $version->id], 'method' => 'PUT', 
            'files' => true]) !!}
    @else
        {!! Form::open(['route' => 'versions.store', 'files' => true]) !!}
    @endif
    
   
    <div class="row">
        <div class="form-group col-md-6">
            <label for="url">Modelo</label>
            {!! Form::select('model_id', $data, !empty($version->model_id) ? $version->model_id : null, 
                ['class' => 'form-control', 'style' => 'height: 50px !important;', 
                'placeholder' => 'Selecione um modelo...', 'data-live-search' => 'true']) !!}
        </div>
                
        <div class="form-group col-md-6">
            <label for="name">Nome</label>
            {!! Form::text('name', null, ['class' => 'form-control altura', 
                    'placeholder' => 'Informe o nome da versão...']) !!}
        </div>     
    </div>
    
    <br/><br/>
    
    <div class="row">    
        <div class="text-right col-xs-12">
            <div class="hidden-xs">
                <button type="submit" class="btn btn-primary"><i class="fa fa-refresh"></i> Salvar Dados</button>
            </div>
        </div>
    </div>
    
        
    {!! Form::close() !!}    

@endsection


@push('scripts')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function(){
        	$("select[name='model_id']").select2();
        });
        
     </script>
     
@endpush
