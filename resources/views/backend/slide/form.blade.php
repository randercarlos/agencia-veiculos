
@extends('backend.template.app')

@section('main_title')
    {{ isset($slide) ? 'Editar slide' : 'Novo slide' }}
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/libs/select2/dist/css/select2.css') }}">
@endpush

@section('content')
    
    @include('backend.includes.errors')
    
    
    @if (isset($slide))
        {!! Form::model($slide, ['route' => ['slides.update', $slide->id], 'method' => 'PUT', 'files' => true]) !!}
    @else
        {!! Form::open(['route' => 'slides.store', 'files' => true]) !!}
    @endif
    
    <div class="row">
        <div class="form-group col-md-6">
            <label for="name">Título</label>
                {!! Form::text('name', null, ['class' => 'form-control altura', 
                    'placeholder' => 'Informe o nome do slide']) !!}
        </div>
        
        <div class="form-group col-md-4">
            <label for="link">Link</label>
            {!! Form::text('link', null, ['class' => 'form-control altura', 
                'placeholder' => 'http://www.nomedosite.com.br']) !!}
        </div>

        <div class="col-md-2 col-xs-12">
            <label for="new">Nova Página ?</label>
            {!!  Form::select('new', ['1' => 'Sim', '0' => 'Não'], '1', ['class' => 'form-control']) !!}
        </div>
        
    </div>
    
    <br/><br/>
      
    <div class="form-group row">   
        <div class="col-md-10">   
            <span class="btn btn-black btn-file col-xs-12 col-md-4 ">
                <i class="fa fa-cloud-upload"></i>
                  
                Selecione um Foto {!! Form::file('photo') !!}
            </span>
            
            &nbsp; &nbsp; &nbsp;
             
            <span class="text-muted" style="line-height: 50px">{{-- Dimensões:  120 Largura x 60 Altura (pixels) --}}
                </span>
        </div>
        
        <div class="col-md-2">
            @if (isset($slide) && !empty($slide->photo))                            
                <p class="text-right">
                    <img class="img-thumbnail" style="width: 120px; height:60px; "
                         src="{{ asset('uploads/slide/' . $slide->photo) }}" />
                </p>    
            @endif
        </div>
        
    </div>
    
    <div class="row">    
        <div class="text-right col-xs-12">
            <div class="hidden-xs">
                <button type="submit" class="btn btn-primary"><i class="fa fa-refresh"></i> Salvar Dados</button>
            </div>
        </div>
    </div>
        
    {!! Form::close() !!}    

@endsection

@push('scripts')

    <script type="text/javascript" src="{{ asset('assets/libs/select2/dist/js/select2.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){

            $("select[name='new']").select2();
            
        });
     </script>
     
@endpush