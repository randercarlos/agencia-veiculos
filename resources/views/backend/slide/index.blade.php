
@extends('backend.template.app')

@section('main_title', 'Slides')

@section('content')
    
    
    @include('backend.includes.alerts')
    
    
    <p class="text-right">
        <a href="{{ route('slides.create') }}" class="btn btn-primary">
            <i class="fa fa-plus-circle"></i> Cadastrar Novo
        </a>
    </p>
    
    
    <table class="table table-striped table-bordered" id="tb1">
        <tr>
            <th width="12%">Foto</th>
            <th width="25%">Título</th>
            <th width="42%">Link</th>
            <th width="10%" class="text-center">Nova Página ?</th>
            <th class="text-center" width="10%">
                <i class="fa fa-cog"></i>
            </th>
        </tr>
        
        @forelse($slides as $slide)        
            <tr>
                <td>
                    <img src="{{ asset('uploads/slide/' . $slide->photo) }}" style='width: 120px; height: 60px' />
                </td>
                <td style="vertical-align: middle;">{{ $slide->name }}</td>
                <td style="vertical-align: middle;">{{ $slide->link }}</td>
                <td style="vertical-align: middle;" class="text-center">{{ $slide->new }}</td>
                <td class="text-center" style="vertical-align: middle;">
                 
                    <a href="{{ route('slides.edit', $slide->id) }}" class="btn btn-primary btn-sm" 
                        data-toggle="tooltip" title="editar">
                        <i class="fa fa-edit"></i>
                    </a>
                    
                    <button data-link="{{ route('slides.destroy', $slide->id) }}" data-toggle="tooltip" 
                        title="remover" class="btn btn-danger btn-sm btn-remover" data-resource="{{ $slide->name }}">
                            <i class="fa fa-trash"></i> 
                    </button>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan='4' style='text-align: center;'>Nenhum registro encontrado!</td>            
            </tr>
        @endforelse
        
    </table>
    
    <div class="row">
        <div class="col-md-12 text-center">
            {{ $slides->links() }}        
        </div>
    </div>
    
    @include('backend.includes.modal_delete', ['title' => 'slide', 'text' => 'o slide', 'route' => 'slides.destroy'])
    
    
@endsection


@push('scripts')
    
    <!-- inclui o javascript necessário para exiber o modal para confirmação da exclusão  -->  
    <script type="text/javascript" src="{{ asset('assets/js/delete_actions.js') }}"></script>
     
@endpush