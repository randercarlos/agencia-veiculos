
@extends('backend.template.app')

@section('main_title')
    {{ isset($optional) ? 'Editar opcional' : 'Novo opcional' }}
@endsection

@push('styles')
    <link rel="stylesheet" 
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
@endpush


@section('content')
    
    @include('backend.includes.errors')
    
    
    @if (isset($optional))
        {!! Form::model($optional, ['route' => ['optionals.update', $optional->id], 'method' => 'PUT']) !!}
    @else
        {!! Form::open(['route' => 'optionals.store']) !!}
    @endif
    
   
    <div class="row">
        <div class="form-group col-md-6">
            <label for="url">Grupo</label>
            {!! Form::select('group_id', $groups, !empty($optional->group_id) ? $optional->group_id : null, 
                ['class' => 'form-control selectpicker show-tick', 
                'placeholder' => 'Selecione um grupo...', 'data-live-search' => 'true']) !!}
        </div>
                
        <div class="form-group col-md-6">
            <label for="name">Nome</label>
            {!! Form::text('name', null, ['class' => 'form-control altura', 
                    'placeholder' => 'Informe o nome do opcional...']) !!}
        </div>     
    </div>
    
    <br/><br/>
    
    <div class="row">    
        <div class="text-right col-xs-12">
            <div class="hidden-xs">
                <a href="{{ route('optionals.index') }}" class="btn btn-warning"> 
                    <i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar a Listagem
                </a>
                <button type="submit" class="btn btn-primary"><i class="fa fa-refresh"></i> Salvar Dados</button>
            </div>
        </div>
    </div>
    
        
    {!! Form::close() !!}    

@endsection


@push('scripts')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function(){
        	$("select[name='group_id']").selectpicker({
                style: null    
            });
        });
        
     </script>
     
@endpush
