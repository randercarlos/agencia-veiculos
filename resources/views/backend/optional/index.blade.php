
@extends('backend.template.app')

@section('main_title')

    @if(isset($group))
        Opcionais do Grupo "{{ $group->name }}"
    @else
        Opcionais
    @endif
    
@endsection

@section('content')
    
    
    @include('backend.includes.alerts')
    
    
    <p class="text-right">
    
        @if (isset($group)) 
            <a href="{{ route('groups.index') }}" class="btn btn-warning">
                Voltar para a listagem de grupos
            </a>
        @endif
    
        <a href="{{ route('optionals.create') }}" class="btn btn-primary">
            <i class="fa fa-plus-circle"></i> Cadastrar Novo
        </a>
    </p>
    
    
    <table class="table table-striped table-bordered" id="tb1">
        <tr>
            <th>Nome</th>
            <th width="25%">Grupo</th>
            <th class="text-center" width="150px">
                <i class="fa fa-cog"></i>
            </th>
        </tr>
        
        @forelse($optionals as $optional)        
            <tr>
                <td> {{ $optional->name }}</td>
                <td> {{ $optional->group->name }}</td>
                <td class="text-center">
                
                    <a href="{{ route('optionals.edit', $optional->id) }}" class="btn btn-primary btn-sm" 
                        data-toggle="tooltip" title="editar">
                        <i class="fa fa-edit"></i>
                    </a>
                    
                    <button data-link="{{ route('optionals.destroy', $optional->id) }}" data-toggle="tooltip" 
                        data-resource="{{ $optional->name }}" class="btn btn-danger btn-sm btn-remover" 
                        title="remover">
                            <i class="fa fa-trash"></i> 
                    </button>
                </td>
            </tr>
        @empty
        
            <tr>
                <td colspan='3' style='text-align: center;'>Nenhum registro encontrado!</td>            
            </tr>
            
        @endforelse
    </table>
    
    
    <div class="row">
        <div class="col-md-12 text-center">
            {{ $optionals->links() }}
        </div>
    </div>
    
    @include('backend.includes.modal_delete', ['title' => 'opcional', 'text' => 'o opcional', 
       'route' => 'optionals.destroy'])
    
    
@endsection


@push('scripts')
    
    <!-- inclui o javascript necessário para exiber o modal para confirmação da exclusão  -->  
    <script type="text/javascript" src="{{ asset('assets/js/delete_actions.js') }}"></script>
     
@endpush