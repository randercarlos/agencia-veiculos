
@extends('backend.template.app')

@section('main_title', 'Configuração de Email')

@section('content')

    {!! Form::model($smtp, ['route' => ['smtp.update'], 'method' => 'PUT']) !!}

    {!! csrf_field() !!}
    
    
    @include('backend.includes.alerts')
    @include('backend.includes.errors')
    
    
    <div class="row">
        <div class="form-group col-md-4">
            <label for="host">Host:</label>
            {!! Form::text('host', null, ['class' => 'form-control altura', 'placeholder' => 'Informe o host']) !!}
        </div>
                     
        <div class="form-group col-md-4">
            <label for="email">Email:</label>
            {!! Form::email('email', null, ['class' => 'form-control altura', 'placeholder' => 'Informe o email']) !!}
        </div>
               
        <div class="form-group col-md-4">
            <label for="password">Senha(Informe se quiser alterar):</label>
             {!! Form::password('password', ['class' => 'form-control altura', 
                'placeholder' => 'Informe a senha']) !!}
        </div>
        
        <div class="form-group col-md-4">
            <label for="name">Nome:</label>
            {!! Form::text('name', null, ['class' => 'form-control altura', 
                'placeholder' => 'Informe o nome']) !!}
        </div>
        
        <div class="form-group col-md-4">
            <label for="bcc">Com cópia:</label>
            {!! Form::text('bcc', null, ['class' => 'form-control altura', 'placeholder' => 'Com cópia para']) !!}
        </div>
                              
        <div class="form-group col-md-3">
            <label for="subject">Assunto:</label>
            {!! Form::text('subject', null, ['class' => 'form-control altura', 'placeholder' => 'Assunto...']) !!}
        </div>
                                 
        <div class="form-group col-md-1">
            <label for="port">Porta:</label>
             {!! Form::text('port', null, ['class' => 'form-control altura', 'placeholder' => 'Informe a porta']) !!}
        </div>
                                 
    </div>
    
    <br/><br/>
    
    <div class="row">    
        <div class="text-right col-xs-12">
            <div class="hidden-xs">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-refresh"></i> Atualizar Dados
                </button>
            </div>
        </div>
    </div>
        
    {!! Form::close() !!}  

@endsection