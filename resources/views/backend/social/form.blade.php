
@extends('backend.template.app')

@section('main_title', 'Redes Sociais')

@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/libs/summernote/summernote.css') }}">
@endpush

@section('content')

    {!! Form::model($social, ['route' => ['social.update'], 'method' => 'PUT']) !!}

    @include('backend.includes.alerts')
    @include('backend.includes.errors')
    
    
    <div class="form-group">
        <label for="facebook"><i class="fa fa-facebook-official"></i> Facebook</label>
        {!! Form::text('facebook', null, ['class' => 'form-control', 'placeholder' => 'Endereço do Facebook...']) !!}
    </div>
    
    <div class="form-group">
        <label for="twitter"><i class="fa fa-twitter-square"></i> Twitter</label>
        {!! Form::text('twitter', null, ['class' => 'form-control', 'placeholder' => 'Endereço do Twitter...']) !!}
    </div>
    
    <div class="form-group">
        <label for="pinterest"><i class="fa fa-pinterest-square"></i> Pinterest</label>
        {!! Form::text('pinterest', null, ['class' => 'form-control', 'placeholder' => 'Endereço do Pinterest...']) !!}
    </div>
    
    <div class="form-group">
        <label for="google"><i class="fa fa-google-plus-square"></i> Google+</label>
        {!! Form::text('google', null, ['class' => 'form-control', 'placeholder' => 'Endereço do Google+...']) !!}
    </div>
    
    <div class="form-group">
        <label for="instagram"><i class="fa fa-instagram"></i> Instagram</label>
        {!! Form::text('instagram', null, ['class' => 'form-control', 'placeholder' => 'Endereço do Instagram...']) !!}
    </div>
    
    <div class="form-group">
        <label for="vimeo"><i class="fa fa-vimeo-square"></i> Vimeo</label>
        {!! Form::text('vimeo', null, ['class' => 'form-control', 'placeholder' => 'Endereço do Vimeo...']) !!}
    </div>
    
    <br />
                     
    <div class="row">    
        <div class="text-right col-xs-12">
            <div class="hidden-xs">
                <button type="submit" class="btn btn-primary"><i class="fa fa-refresh"></i> Atualizar Dados</button>
            </div>
        </div>
    </div>
        
    {!! Form::close() !!}  
    
@endsection

