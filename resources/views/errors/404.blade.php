
@extends('backend.template.app')

@section('main_title', 'Página não encontrada!')

@section('content')
    
    <h1 style="display: flex; align-items: center; justify-content: center; font-size: 44px">
    	Página não encontrada! Voltar para a &nbsp;
    	<a href="{{ route('autos.index') }}">Home</a>
    </h1>
    
    
@endsection

