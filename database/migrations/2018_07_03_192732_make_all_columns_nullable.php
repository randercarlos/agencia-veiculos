<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeAllColumnsNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('social', function (Blueprint $table) {
            $table->string('facebook')->nullable()->change();
            $table->string('twitter')->nullable()->change();
            $table->string('vimeo')->nullable()->change();
            $table->string('instagram')->nullable()->change();
            $table->string('pinterest')->nullable()->change();
            $table->string('google')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('social', function (Blueprint $table) {
            $table->string('facebook')->change();
            $table->string('twitter')->change();
            $table->string('vimeo')->change();
            $table->string('instagram')->change();
            $table->string('pinterest')->change();
            $table->string('google')->change();
        });
    }
}
