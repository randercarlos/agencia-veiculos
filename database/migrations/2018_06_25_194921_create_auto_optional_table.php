<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutoOptionalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auto_optional', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('auto_id');
            $table->unsignedInteger('optional_id');
            
            $table->foreign('auto_id')->references('id')->on('autos')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('optional_id')->references('id')->on('optionals')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auto_optional');
    }
}
