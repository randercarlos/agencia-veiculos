<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('plate');
            $table->string('plate_num');
            $table->string('obs');
            $table->string('photo');
            $table->year('year');
            $table->decimal('price', 10, 2);
            $table->boolean('is_state');
            $table->boolean('is_negotiable');
            $table->integer('km');
            $table->integer('ports');
            $table->boolean('is_featured');
            $table->string('url');
            $table->boolean('is_visitable');
            $table->boolean('is_top');
            $table->boolean('is_offer');
            $table->string('seo_description');
            $table->string('seo_keys');
            
            $table->unsignedInteger('version_id');
            $table->unsignedInteger('brand_id');
            $table->unsignedInteger('model_id');
            $table->unsignedInteger('color_id');
            $table->unsignedInteger('gearbox_id');
            $table->unsignedInteger('fuel_id');
            $table->unsignedInteger('documentation_id');
            $table->unsignedInteger('need_id');
            
            
            $table->foreign('version_id')->references('id')->on('versions')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('brand_id')->references('id')->on('brands')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('model_id')->references('id')->on('models')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('color_id')->references('id')->on('colors')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('gearbox_id')->references('id')->on('gearboxes')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('fuel_id')->references('id')->on('fuels')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('documentation_id')->references('id')->on('documentations')->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->foreign('need_id')->references('id')->on('needs')->onUpdate('cascade')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autos');
    }
}
