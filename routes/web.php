<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});
    

Route::group(['prefix' => '/admin', 'namespace' => 'Backend'], function() {
    
    // Home / Dashboard
    Route::get('/', function(){
        
        return redirect()->route('autos.index');
        
    })->name('admin.index');
    
    
    // Autos
    Route::resource('/autos', 'AutoController');
    
    // Config -> Smtp
    Route::get('/smtp', 'SmtpController@edit')->name('smtp.edit');
    Route::put('/smtp', 'SmtpController@update')->name('smtp.update');
    
    // Config -> Agência
    Route::get('/agencia', 'AgencyController@edit')->name('agency.edit');
    Route::put('/agencia', 'AgencyController@update')->name('agency.update');
    
    // Config -> Usuários
    Route::resource('/users', 'UserController')->except('show');
    
    // Redes Sociais
    Route::get('/socials', 'SocialController@edit')->name('social.edit');
    Route::put('/socials', 'SocialController@update')->name('social.update');
    
    // Depoimentos
    Route::resource('/testemonials', 'TestemonialController')->except('show');
    
    // Serviços
    Route::resource('/services', 'ServiceController')->except('show');
    
    // Slides
    Route::resource('/slides', 'SlideController')->except('show');
    
    // Parceiros
    Route::resource('/partners', 'PartnerController')->except('show');
    
    // Cores
    Route::resource('/colors', 'ColorController')->except('show');
    
    // Câmbios
    Route::resource('/gearboxes', 'GearboxController')->except('show');
    
    // Marcas
    Route::resource('/brands', 'BrandController')->except('show');
    Route::get('/brands/{id}/models', 'BrandController@models')->name('brands.models');
    
    // Modelos
    Route::resource('/models', 'ModelController')->except('show');
    Route::get('/models/{id}/versions', 'ModelController@versions')->name('models.versions');
    
    // Versões
    Route::resource('/versions', 'VersionController')->except('show');
    
    // Grupos de Opcionais
    Route::get('/groups/{id}/optionals', 'GroupController@optionals')->name('groups.optionals');
    Route::resource('/groups', 'GroupController')->except('show');
    
    // Opcionais
    Route::resource('/optionals', 'OptionalController')->except('show');
});
        
