
// exibe o modal para confirmação da exclusão 
$(document).ready(function() {
    
     $('[data-toggle="tooltip"]').tooltip();

     
     $('.btn-remover').on('click', function () {
         
         var link = $(this).data('link');
         var resource = $(this).data('resource');

         $('#name_resource').html(resource);
         $('#modal-remover').modal('show');
         
         $('#form_delete').attr('action', link);
     });

     
 	 //Warning Message
     $('#link-remover').click(function(e){
         
    	  $('#form_delete').submit();
         
     });
});