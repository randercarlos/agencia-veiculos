<?php 

namespace App\Helpers;

use Illuminate\Support\Facades\File;
use Exception;

/**
 * 
 * Classe para upload de arquivos em laravel.
 * 
 */
class Upload {
   
    /**
     * Método execute. Executa o upload do arquivo
     * 
     * @param string $imgInput Nome do campo de upload da imagem no form 
     * @param string $folder Subpasta aonde a imagem será salva dentro da pasta uploads
     * @param string $oldImagePath=null Opcional.Caminho completo da imagem antiga a ser excluída no caso de atualização 
     * 
     * @return string|false O nome da imagem gerado ou false caso ocorra algum erro. 
     */
    public static function execute($imgInput, $folder, $oldImagePath = null) {
            
        if (request()->hasFile($imgInput) && request()->file($imgInput)->isValid()) {
            
            try {
                // recupera o caminho onde a imagem foi salva
                $caminhoUpload = request()->file($imgInput)->store($folder);
            }
            catch(Exception $e) {
                return false;
            }
            
            // recupera o nome da imagem que foi gerado ao salvar a imagem
            $novoNome = explode('/', $caminhoUpload)[1];
            
            // Deleta a imagem antiga depois de fazer o upload da nova
            if ($oldImagePath != null && File::exists($oldImagePath)) {
                File::delete($oldImagePath);
            }
            
            return $novoNome;
        }
        
        return true;
    }
}
