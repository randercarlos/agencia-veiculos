<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AutoFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'plate_num' => 'required|alpha_dash|size:8',
            'plate' => 'required|max:8',
            'obs' => 'required|max:500',
            'photo',
            'year' => 'required|integer',
            'price' => 'required|numeric|max:999999',
            'is_state' => 'required|boolean',
            'is_negotiable' => 'nullable',
            'is_visitable' => 'nullable',
            'is_featured' => 'required|boolean',
            'is_top' => 'nullable',
            'is_offer' => 'required|boolean',
            'km' => 'required|integer',
            'ports' => 'required|integer',
            'url' => 'alpha_dash|max:100',
            'seo_description' => 'alpha_dash|max:100',
            'seo_keys' => 'alpha_dash|max:100',
            'version_id' => 'required|integer|exists:versions,id',
            'brand_id' => 'required|integer|exists:brands,id',
            'model_id' => 'required|integer|exists:models,id',
            'color_id' => 'required|integer|exists:colors,id',
            'gearbox_id' => 'required|integer|exists:gearboxes,id',
            'fuel_id' => 'required|integer|exists:fuels,id',
            'documentation_id' => 'required|integer|exists:documentations,id',
            'need_id' => 'required|integer|exists:needs,id',
            'bodywork_id' => 'required|integer|exists:bodyworks,id'
        ];
    }
}
