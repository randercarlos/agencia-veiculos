<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Auto;
use App\Services\AutoService;
use Illuminate\Http\Request;
use App\Http\Requests\AutoFormRequest;

class AutoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$autos = AutoService::findAll();

        $autos = Auto::join('brands', 'autos.brand_id', '=', 'brands.id')
                        ->join('models', 'models.id', '=', 'autos.model_id')
                        ->join('versions', 'versions.id', '=', 'autos.version_id')
                        ->select('autos.id as auto_id', 'models.name as model_name', 'versions.name as version_name',
                            'brands.name as brand_name', 'autos.photo', 'autos.is_featured')
                        ->paginate(config('constants.NUM_RECORDS_PER_PAGE'));
        
        return view('backend.auto.index', compact('autos', 'brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brands = AutoService::getBrands();
        $models = AutoService::getModels();
        $years = AutoService::getYears();
        $gearboxes = AutoService::getGearboxes();
        $colors = AutoService::getColors();
        $fuels = AutoService::getFuels();
        $bodyworks = AutoService::getBodyworks();
        $documentations = AutoService::getDocumentations();
        $needs = AutoService::getNeeds();
        $plates = AutoService::getPlates();
        $ports = AutoService::getPorts();
        $negotiableTypes = AutoService::getNegotiableTypes();
        $optionalsGroup = AutoService::getOptionalsGroup();
        $optionals = AutoService::getOptionals();
        $versions = AutoService::getVersions();


        return view('backend.auto.form', compact('brands', 'models', 'years', 'gearboxes', 'colors', 'fuels',
            'bodyworks', 'documentations', 'needs', 'plates', 'ports', 'negotiableTypes', 'optionals',
            'optionalsGroup', 'versions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Auto $auto)
    {
        //$request->replace(['km' => intval(str_replace('.', '', $request->input('km')))]);
        //dd($request->all());
        $request->replace(['km' => intval(str_replace('.', '', $request->input('km')))]);
        $request->replace(['price' => floatval(str_replace('R$ ', '', str_replace('.', '', str_replace(',', '.', $request->input('price')))))]);

        $request->validate(Auto::validationRules());

        if ($auto->create($request->all())) {

            // salvar os opcionais na tabela de auto_optional

            return redirect()->route('autos.index')->with('success', 'Auto <b>' . '' . '</b> criado com sucesso!');
        }

        return redirect()->back()->with('error', 'Falha ao criar auto!')->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Auto  $auto
     * @return \Illuminate\Http\Response
     */
    public function show(Auto $auto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Auto  $auto
     * @return \Illuminate\Http\Response
     */
    public function edit(Auto $auto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Auto  $auto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Auto $auto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Auto  $auto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Auto $auto)
    {
        //
    }
}
