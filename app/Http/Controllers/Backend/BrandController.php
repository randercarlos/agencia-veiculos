<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\Upload;
use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brand::orderBy('name', 'asc')->paginate(config('constants.NUM_RECORDS_PER_PAGE'));
        
        return view('backend.brand.index', compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.brand.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Brand $brand)
    {
        $request->validate([
            'name' => 'required|min:3|max:60|unique:brands',
            'url' => 'required|url|min:5|max:100',
            'photo' => 'mimes:jpeg,bmp,png|dimensions:max_width=150,max_height=100',
        ]);
        
        $novoNomeArquivo = Upload::execute('photo', 'brand');
        
        // se o resultado retornar false, é porque houve algum erro ao se fazer o upload
        if ($novoNomeArquivo === false) {
            return redirect()->back()->with('error', 'Falha ao fazer upload da foto!')->withInput();
        }
        
        // recupera os dados da requisição e adiciona o nome gerado no upload da imagem ao atributo photo
        $dadosRequisicao = $request->all();
        $dadosRequisicao['photo'] = $novoNomeArquivo;
        
        if ($brand->create($dadosRequisicao))
        {
            return redirect()->route('brands.index')->with('success', 'Marca criada com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao criar marca!')->withInput();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        return view('backend.brand.form', compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand)
    {
        $id = $brand->id;
        $request->validate([
            'name' => "required|min:3|max:60|unique:brands,name,$id,id",
            'url' => 'required|url|min:5|max:100',
            'photo' => 'required|mimes:jpeg,bmp,png|dimensions:max_width=150,max_height=100',
        ]);
        
        // recupera o caminho completo da imagem antiga salva no BD para exclusão
        $pathOldPhoto = public_path() . '/uploads/brand/' . $brand->photo;
        
        $novoNomeArquivo = Upload::execute('photo', 'brand', $pathOldPhoto);
        
        // se o resultado retornar false, é porque houve algum erro ao se fazer o upload
        if ($novoNomeArquivo === false) {
            return redirect()->back()->with('error', 'Falha ao fazer upload da foto!')->withInput();
        }
        
        // recupera os dados da requisição e adiciona o nome gerado no upload da imagem ao atributo photo
        $dadosRequisicao = $request->all();
        $dadosRequisicao['photo'] = $novoNomeArquivo;
        
        if ($brand->update($dadosRequisicao))
        {
            return redirect()->route('brands.index')->with('success', 'Marca atualizada com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao atualizar marca!')->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brand $brand)
    {
        $pathOldPhoto = public_path() . '/uploads/brand/' . $brand->photo;
        if ($brand->delete()) {
            
            if (File::exists($pathOldPhoto)) {
                File::delete($pathOldPhoto);
            }
            
            return redirect()->route('brands.index')->with('success', 'Marca removida com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao remover marca!')->withInput();
    }
    
    public static function models(Request $request, $brand_id = null)
    {
        if (!is_null($brand_id)) {
            return Model::where('brand_id', $brand_id)->orderby('name')->get();
        }
        
        return Model::orderby('name')->pluck('name', 'id')->get();
    }
}
