<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Helpers\Upload;
use App\Http\Controllers\Controller;
use App\Models\Partner;

class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $partners = Partner::paginate(config('constants.NUM_RECORDS_PER_PAGE'));
        
        return view('backend.partner.index', compact('partners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.partner.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Partner $partner)
    {
        $request->validate([
            'name' => 'required|min:5|max:100',
            'link' => 'required|url|min:10|max:100',
            'new' => 'required|in:1,0',
            'photo' => 'mimes:jpeg,bmp,png,gif|dimensions:width=300,height=300|max:512'
        ]);
        
        
        $novoNomeArquivo = Upload::execute('photo', 'partner');
        
        // se o resultado retornar false, é porque houve algum erro ao se fazer o upload
        if ($novoNomeArquivo === false) {
            return redirect()->back()->with('error', 'Falha ao fazer upload da foto!')->withInput();
        }
        
        // recupera os dados da requisição e adiciona o nome gerado no upload da imagem ao atributo photo
        $dadosRequisicao = $request->all();
        $dadosRequisicao['photo'] = $novoNomeArquivo;
        $dadosRequisicao['local'] = 1;
        
        if ($partner->create($dadosRequisicao))
        {
            return redirect()->route('partners.index')->with('success', 'Parceiro criado com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao criar parceiro!')->withInput();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Partner $partner)
    {
        return view('backend.partner.form', compact('partner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Partner $partner)
    {
        $request->validate([
            'name' => 'required|min:5|max:100',
            'link' => 'required|url|min:10|max:100',
            'new' => 'required|in:1,0',
            'photo' => 'mimes:jpeg,bmp,png,gif|dimensions:width=300,height=300|max:512'
        ]);
        
        // recupera o caminho completo da imagem antiga salva no BD para exclusão
        $pathOldPhoto = public_path() . '/uploads/partner/' . $partner->photo;
        
        $novoNomeArquivo = Upload::execute('photo', 'partner', $pathOldPhoto);
        
        // se o resultado retornar false, é porque houve algum erro ao se fazer o upload
        if ($novoNomeArquivo === false) {
            return redirect()->back()->with('error', 'Falha ao fazer upload da foto!')->withInput();
        }
        
        // recupera os dados da requisição e adiciona o nome gerado no upload da imagem ao atributo photo
        $dadosRequisicao = $request->all();
        $dadosRequisicao['photo'] = $novoNomeArquivo;
        $dadosRequisicao['local'] = 1;
        
        if ($partner->update($dadosRequisicao))
        {
            return redirect()->route('partners.index')->with('success', 'Parceiro atualizado com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao atualizar parceiro!')->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Partner $partner)
    {
        $pathOldPhoto = public_path() . '/uploads/partner/' . $partner->photo;
        if ($partner->delete()) {
            
            if (File::exists($pathOldPhoto)) {
                File::delete($pathOldPhoto);
            }
            
            return redirect()->route('partners.index')->with('success', 'Parceiro removido com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao remover slide!')->withInput();
    }
}
