<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\Upload;
use App\Http\Controllers\Controller;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::orderBy('created_at', 'desc')->paginate(config('constants.NUM_RECORDS_PER_PAGE'));
        
        return view('backend.service.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.service.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Service $service)
    {
        $request->validate([
            'name' => 'required|min:5|max:100',
            'description' => 'required|min:10|max:1000',
            'photo' => 'mimes:jpeg,bmp,png|dimensions:max_width=60,max_height=60',
        ]);
        
        
        $novoNomeArquivo = Upload::execute('photo', 'service');
        
        // se o resultado retornar false, é porque houve algum erro ao se fazer o upload
        if ($novoNomeArquivo === false) {
            return redirect()->back()->with('error', 'Falha ao fazer upload da foto!')->withInput();
        }
        
        // recupera os dados da requisição e adiciona o nome gerado no upload da imagem ao atributo photo
        $dadosRequisicao = $request->all();
        $dadosRequisicao['photo'] = $novoNomeArquivo;
        
        if ($service->create($dadosRequisicao))
        {
            return redirect()->route('services.index')->with('success', 'Serviço criado com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao criar serviço!')->withInput();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        return view('backend.service.form', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        $request->validate([
            'name' => 'required|min:5|max:100',
            'description' => 'required|min:10|max:1000',
            'photo' => 'mimes:jpeg,bmp,png|dimensions:max_width=60,max_height=60',
        ]);
        
        // recupera o caminho completo da imagem antiga salva no BD para exclusão
        $pathOldPhoto = public_path() . '/uploads/service/' . $service->photo;
        
        $novoNomeArquivo = Upload::execute('photo', 'service', $pathOldPhoto);
        
        // se o resultado retornar false, é porque houve algum erro ao se fazer o upload
        if ($novoNomeArquivo === false) {
            return redirect()->back()->with('error', 'Falha ao fazer upload da foto!')->withInput();
        }
        
        // recupera os dados da requisição e adiciona o nome gerado no upload da imagem ao atributo photo
        $dadosRequisicao = $request->all();
        $dadosRequisicao['photo'] = $novoNomeArquivo;
        
        if ($service->update($dadosRequisicao))
        {
            return redirect()->route('services.index')->with('success', 'Serviço alterado com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao alterar serviço!')->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        $pathOldPhoto = public_path() . '/uploads/service/' . $service->photo;
        if ($service->delete()) {
            
            if (File::exists($pathOldPhoto)) {
                File::delete($pathOldPhoto);
            }
            
            return redirect()->route('services.index')->with('success', 'Serviço removido com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao remover serviço!')->withInput();
    }
}
