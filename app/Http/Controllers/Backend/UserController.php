<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('name')->paginate(config('constants.NUM_RECORDS_PER_PAGE'));
        
        return view('backend.user.index', compact('users'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.user.form');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:5|max:60',
            'email' => 'required|email|max:40|unique:users,email',
            'password' => 'required|min:5|max:10',
            'password_confirm' => 'required|same:password',
        ]);
        
        if (User::create($request->all()))
        {
            return redirect()->route('users.index')->with('success', 'Usuário <b>' . $request->input('name') 
                    . '</b> criado com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao criar usuário!')->withInput();
    }
    
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        
        return view('backend.user.form', compact('user'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $request->validate([
            'name' => 'required|min:5|max:60',
            'email' => "required|email|max:40|unique:users,email,$id",
            'password' => 'nullable|min:5|max:10',
            'password_confirm' => 'required_with:password|same:password',
        ]);
    
        $user = User::find($id);
        
        if ($user->update($request->all()))
        {
            return redirect()->route('users.index')->with('success', 'Usuário <b>' . $request->input('name')
                . '</b> foi atualizado com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao atualizar usuário!')->withInput();
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $name = $user->name;
        
        if ($user->delete())
        {
            return redirect()->route('users.index')->with('success', 'Usuário <b>' . $name
                . '</b> foi removido com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao remover usuário!')->withInput();
    }
}
