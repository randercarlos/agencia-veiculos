<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Color;
use Illuminate\Http\Request;

class ColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $colors = Color::orderBy('name', 'asc')->paginate(config('constants.NUM_RECORDS_PER_PAGE'));
        
        return view('backend.color.index', compact('colors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.color.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Color $color)
    {
        $request->validate([
            'name' => 'required|min:3|max:20|unique:colors',
        ]);
        
        
        if ($color->create($request->all()))
        {
            return redirect()->route('colors.index')->with('success', 'Cor criada com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao criar cor!')->withInput();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Color  $color
     * @return \Illuminate\Http\Response
     */
    public function edit(Color $color)
    { 
        return view('backend.color.form', compact('color'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Color  $color
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Color $color)
    {
        $request->validate([
            'name' => 'required|min:3|max:20|unique:colors',
        ]);
        
        
        if ($color->update($request->all()))
        {
            return redirect()->route('colors.index')->with('success', 'Cor atualizada com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao atualizar cor!')->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Color  $color
     * @return \Illuminate\Http\Response
     */
    public function destroy(Color $color)
    {
        if ($color->delete()) {
            
            return redirect()->route('colors.index')->with('success', 'Cor removida com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao remover cor!')->withInput();
    }
}
