<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Version;
use Illuminate\Http\Request;

class VersionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*$versions = DB::table('versions')
        ->join('models', 'versions.model_id', '=', 'models.id')
        ->join('brands', 'models.brand_id', '=', 'brands.id')
        ->select('versions.*', 'models.name AS model_name', 'brands.name AS brand_name', 'brands.photo AS brand_photo')
        ->get(); */
        
        $versions = Version::with('model.brand')->orderBy('name', 'asc')
            ->paginate(config('constants.NUM_RECORDS_PER_PAGE'));
        
        return view('backend.version.index', compact('versions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Version $version)
    {
        $data = $version->getModelsWithBrand();
        
        return view('backend.version.form', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Version $version)
    {
        $request->validate([
            'name' => 'required|min:5|max:100|unique:versions,name',
            'model_id' => 'required|integer|exists:models,id',
        ]);
        
        
        if ($version->create($request->all()))
        {
            return redirect()->route('versions.index')->with('success', 'Versão <b>' . $request->name
                . '</b> criada com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao criar versão!')->withInput();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Version  $version
     * @return \Illuminate\Http\Response
     */
    public function edit(Version $version)
    {
        $data = $version->getModelsWithBrand();
        
        return view('backend.version.form', compact('version', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Version  $version
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Version $version)
    {
        $id = $version->id;
        $request->validate([
            'name' => "required|min:5|max:100|unique:versions,name,$id",
            'model_id' => 'required|integer|exists:models,id',
        ]);
        
        
        if ($version->update($request->all()))
        {
            return redirect()->route('versions.index')->with('success', 'Versão <b>' . $request->name
                . '</b> foi atualizada com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao atualizar versão!')->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Version  $version
     * @return \Illuminate\Http\Response
     */
    public function destroy(Version $version)
    {
        $name = $version->name;
        if ($version->delete()) {
            return redirect()->route('versions.index')->with('success', "Versão <b>$name</b> removida com sucesso!");
        }
        
        return redirect()->back()->with('error', 'Falha ao remover versão!')->withInput();
    }
}
