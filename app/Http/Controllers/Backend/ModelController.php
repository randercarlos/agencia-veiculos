<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Model;
use App\Models\Version;
use Illuminate\Http\Request;

class ModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = Model::with('brand')->orderBy('name', 'asc')->paginate(config('constants.NUM_RECORDS_PER_PAGE'));
        
        return view('backend.model.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brands = Brand::orderBy('name', 'asc')->pluck('name', 'id');
        
        return view('backend.model.form', compact('brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Model $model)
    {
        $request->validate([
            'name' => 'required|min:3|max:60|unique:models,name',
            'brand_id' => 'required|integer|exists:brands,id',
        ]);
        
        
        if ($model->create($request->all()))
        {
            return redirect()->route('models.index')->with('success', 'Modelo <b>' . $request->name 
                . '</b> criado com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao criar modelo!')->withInput();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Model $model)
    {
        $brands = Brand::orderBy('name', 'asc')->pluck('name', 'id');
        
        return view('backend.model.form', compact('model', 'brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Model $model)
    {
        $id = $model->id;
        $request->validate([
            'name' => "required|min:3|max:60|unique:models,name,$id",
            'brand_id' => 'required|integer|exists:brands,id',
        ]);
        
        
        if ($model->update($request->all())) {
            return redirect()->route('models.index')->with('success', 'Modelo <b>' . $request->name
                . '</b> atualizado com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao criar modelo!')->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Model $model)
    {
        $name = $model->name;
        if ($model->delete()) {
            return redirect()->route('models.index')->with('success', "Modelo <b>$name</b> removido com sucesso!");
        }
        
        return redirect()->back()->with('error', 'Falha ao remover modelo!')->withInput();
    }
    
    public function getVersions(Model $model)
    {
        return $model->versions()->get();
    }
    
    public static function versions(Request $request, $model_id = null)
    {
        if (!is_null($model_id)) {
            return Version::where('model_id', $model_id)->orderby('name')->get();
        }
        
        return Version::orderby('name')->get();
    }
}
