<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Gearbox;
use Illuminate\Http\Request;

class GearboxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gearboxes = Gearbox::orderBy('name', 'asc')->paginate(config('constants.NUM_RECORDS_PER_PAGE'));
        
        return view('backend.gearbox.index', compact('gearboxes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.gearbox.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Gearbox $gearbox)
    {
        $request->validate([
            'name' => 'required|min:5|max:60|unique:gearboxes',
        ]);
        
        
        if ($gearbox->create($request->all()))
        {
            return redirect()->route('gearboxes.index')->with('success', 'Câmbio criado com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao criar câmbio!')->withInput();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Gearbox  $gearbox
     * @return \Illuminate\Http\Response
     */
    public function edit(Gearbox $gearbox)
    {
        return view('backend.gearbox.form', compact('gearbox'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Gearbox  $gearbox
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Gearbox $gearbox)
    {
        $request->validate([
            'name' => 'required|min:5|max:60|unique:colors',
        ]);
        
        
        if ($gearbox->update($request->all()))
        {
            return redirect()->route('gearboxes.index')->with('success', 'Câmbio atualizado com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao atualizar câmbio!')->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Gearbox  $gearbox
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gearbox $gearbox)
    {
        if ($gearbox->delete()) {
            
            return redirect()->route('gearboxes.index')->with('success', 'Câmbio removido com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao remover câmbio!')->withInput();
    }
}
