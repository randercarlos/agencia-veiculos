<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\Upload;
use App\Http\Controllers\Controller;
use App\Models\Agency;
use Illuminate\Http\Request;

class AgencyController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Agency  $agency
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $agency = Agency::first();
        
        return view('backend.agency.form', compact('agency'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Agency  $agency
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Agency $agency)
    {
        $request->validate([
            'name' => 'required|min:10|max:100',
            'phone' => 'required|min:13|max:15',
            'phone2' => 'nullable|min:13|max:15',
            'phone3' => 'nullable|min:13|max:15',
            'address' => 'required|min:5|max:200',
            'business_hours' => 'required|min:5|max:60',
            'title' => 'required|min:5|max:60',
            'description' => 'required|min:10|max:1000',
            'seo_description' => 'min:10|max:100',
            'seo_keys' => 'min:5|max:30',
            'logotipo' => 'mimes:jpeg,bmp,png|dimensions:max_width=210,max_height=40',
        ]);
        
        // recupera o primeiro registro da tabela agencies, pois sempre haverá apenas 1 único registro
        $agency = Agency::first();
        
        // recupera o caminho completo da imagem antiga salva no BD para exclusão
        $pathOldPhoto = public_path() . '/uploads/agency/' . $agency->photo;
        
        // executa o upload e retorna o resultado. Se retorna o nome do arquivo, é porque o upload foi bem-sucedido
        $novoNomeArquivo = Upload::execute('logotipo', 'agency', $pathOldPhoto);
        
        // se o resultado retornar false, é porque houve algum erro ao se fazer o upload
        if ($novoNomeArquivo === false) {
            return redirect()->back()->with('error', 'Falha ao fazer upload da foto!')->withInput();
        }
        
        // recupera os dados da requisição e adiciona o nome gerado no upload da imagem ao atributo photo
        $dadosRequisicao = $request->all();
        $dadosRequisicao['photo'] = $novoNomeArquivo;
        
        if ($agency->update($dadosRequisicao))
        {
            return redirect()->route('agency.edit')->with('success', 'Agência atualizada com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao atualizar Agência!')->withInput();
    }
}
