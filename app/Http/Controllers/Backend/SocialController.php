<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Social;
use Illuminate\Http\Request;

class SocialController extends Controller
{

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Social  $social
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $social = Social::first();
        
        return view('backend.social.form', compact('social'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Social  $social
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Social $social)
    {
        $request->validate([
            'facebook' => 'nullable|url|min:10|max:60', 
            'twitter' => 'nullable|url|min:10|max:60', 
            'pinterest' => 'nullable|url|min:10|max:60', 
            'google' => 'nullable|url|min:10|max:60', 
            'instagram' => 'nullable|url|min:10|max:60', 
            'vimeo' => 'nullable|url|min:10|max:60'
        ]);
        
        // recupera o primeiro e único registro da tabela socials(deve haver apenas um único registro nela)
        $social = Social::first();
        
        
        // se existir o registro na tabela, atualiza(update). Se não existir, cria(create) o registro
        $resultado = !is_null($social) ? $social->update($request->except('_method', '_token')) 
            : $social->create($request->except('_method', '_token'));
            
        // se o resultado for true, quer dizer que a execução foi bem sucedida e exibe a mensagem de sucesso
        if ($resultado) {
            return redirect()->route('social.edit')->with('success', '<b>Redes Sociais</b> atualizadas com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao atualizar Redes Sociais!')->withInput();
    }
}
