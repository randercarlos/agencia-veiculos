<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\Upload;
use App\Http\Controllers\Controller;
use App\Models\Slide;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slides = Slide::where('local', 1)->paginate(config('constants.NUM_RECORDS_PER_PAGE'));
        
        return view('backend.slide.index', compact('slides'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.slide.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Slide $slide)
    {
        $request->validate([
            'name' => 'required|min:5|max:100',
            'link' => 'required|url|min:10|max:100',
            'new' => 'required|in:1,0',
            'photo' => 'mimes:jpeg,bmp,png,gif|dimensions:max_width=1600,max_height=500|max:512'
        ]);
        
        
        $novoNomeArquivo = Upload::execute('photo', 'slide');
        
        // se o resultado retornar false, é porque houve algum erro ao se fazer o upload
        if ($novoNomeArquivo === false) {
            return redirect()->back()->with('error', 'Falha ao fazer upload da foto!')->withInput();
        }
        
        // recupera os dados da requisição e adiciona o nome gerado no upload da imagem ao atributo photo
        $dadosRequisicao = $request->all();
        $dadosRequisicao['photo'] = $novoNomeArquivo;
        $dadosRequisicao['local'] = 1;
        
        if ($slide->create($dadosRequisicao))
        {
            return redirect()->route('slides.index')->with('success', 'Slide criado com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao criar slide!')->withInput();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Slide  $slide
     * @return \Illuminate\Http\Response
     */
    public function edit(Slide $slide)
    {
        return view('backend.slide.form', compact('slide'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Slide  $slide
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slide $slide)
    {
        $request->validate([
            'name' => 'required|min:5|max:100',
            'link' => 'required|url|min:10|max:100',
            'new' => 'required|in:1,0',
            'photo' => 'mimes:jpeg,bmp,png,gif|dimensions:max_width=1600,max_height=500|max:512'
        ]);
        
        // recupera o caminho completo da imagem antiga salva no BD para exclusão
        $pathOldPhoto = public_path() . '/uploads/slide/' . $slide->photo;
        
        $novoNomeArquivo = Upload::execute('photo', 'slide', $pathOldPhoto);
        
        // se o resultado retornar false, é porque houve algum erro ao se fazer o upload
        if ($novoNomeArquivo === false) {
            return redirect()->back()->with('error', 'Falha ao fazer upload da foto!')->withInput();
        }
        
        // recupera os dados da requisição e adiciona o nome gerado no upload da imagem ao atributo photo
        $dadosRequisicao = $request->all();
        $dadosRequisicao['photo'] = $novoNomeArquivo;
        $dadosRequisicao['local'] = 1;
        
        if ($slide->update($dadosRequisicao))
        {
            return redirect()->route('slides.index')->with('success', 'Slide criado com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao criar slide!')->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Slide  $slide
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slide $slide)
    {
        $pathOldPhoto = public_path() . '/uploads/slide/' . $slide->photo;
        if ($slide->delete()) {
            
            if (File::exists($pathOldPhoto)) {
                File::delete($pathOldPhoto);
            }
            
            return redirect()->route('slides.index')->with('success', 'Slide removido com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao remover slide!')->withInput();
    }
}
