<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Models\Optional;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Group::orderBy('name')->paginate(config('constants.NUM_RECORDS_PER_PAGE'));
        
        return view('backend.group.index', compact('groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.group.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Group $group)
    {
        $request->validate([
            'name' => 'required|min:3|max:60|unique:groups,name',
        ]);
        
        
        if ($group->create($request->all())) {
            return redirect()->route('groups.index')->with('success', 'O grupo <b>' . $request->name
                . '</b> foi criado com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao criar grupo!')->withInput();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function edit(Group $group)
    {
        return view('backend.group.form', compact('group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Group $group)
    {
        $id = $group->id;
        $request->validate([
            'name' => "required|min:3|max:60|unique:groups,name,$id",
        ]);
        
        
        if ($group->update($request->all())) {
            return redirect()->route('groups.index')->with('success', 'O grupo <b>' . $request->name
                . '</b> foi salvo com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao salvar grupo!')->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group)
    {
        $name = $group->name;
        if ($group->delete()) {
            return redirect()->route('groups.index')->with('success', "Grupo <b>$name</b> foi removido com sucesso!");
        }
        
        return redirect()->back()->with('error', 'Falha ao remover grupo!');
    }
    
    public function optionals($id)
    {
        $group = Group::find($id);
        $optionals = Optional::where('group_id', $group->id)->orderBy('name')
            ->paginate(config('constants.NUM_RECORDS_PER_PAGE'));
        
        return view('backend.optional.index', compact('optionals', 'group'));
    }
}
