<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Smtp;
use Illuminate\Http\Request;

class SmtpController extends Controller
{

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Smtp  $smtp
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $smtp = Smtp::first();
        
        return view('backend.smtp.form', compact('smtp'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Smtp  $smtp
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {   
        $request->validate([
           'name' => 'required|max:60', 
           'host' => 'required|max:60', 
           'email' => 'required|email', 
           'password' => 'sometimes|min:5|max:15', 
           'bcc' => 'required|max:60', 
           'subject' => 'required|max:100', 
           'port' => 'required|integer|min:0|max:65535', 
        ]);
        
        $smtp = Smtp::first();
        if ($smtp->update($request->all()))
        {
            return redirect()->route('smtp.edit')->with('success', 'Smtp atualizado com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao atualizar Smtp!')->withInput();
    }
}
