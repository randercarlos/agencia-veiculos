<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Models\Optional;
use Illuminate\Http\Request;

class OptionalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $optionals = Optional::with('group')->orderBy('name')->paginate(config('constants.NUM_RECORDS_PER_PAGE'));
        
        return view('backend.optional.index', compact('optionals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $groups = Group::orderBy('name')->pluck('name', 'id');
        
        return view('backend.optional.form', compact('groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Optional $optional)
    {
        $request->validate([
            'name' => 'required|min:3|max:60|unique:optionals,name',
            'group_id' => 'required|integer|exists:groups,id',
        ]);
        
        
        if ($optional->create($request->all())) {
            return redirect()->route('optionals.index')->with('success', 'O opcional <b>' . $request->name
                . '</b> foi criado com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao criar opcional!')->withInput();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Optional  $optional
     * @return \Illuminate\Http\Response
     */
    public function edit(Optional $optional)
    {
        $groups = Group::orderBy('name')->pluck('name', 'id');
        
        return view('backend.optional.form', compact('groups', 'optional'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Optional  $optional
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Optional $optional)
    {
        $id = $optional->id;
        $request->validate([
            'name' => "required|min:3|max:60|unique:optionals,name,$id",
            'group_id' => 'required|integer|exists:groups,id',
        ]);
        
        
        if ($optional->update($request->all())) {
            return redirect()->route('optionals.index')->with('success', 'O opcional <b>' . $request->name
                . '</b> atualizado com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao criar opcional!')->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Optional  $optional
     * @return \Illuminate\Http\Response
     */
    public function destroy(Optional $optional)
    {
        $name = $optional->name;
        
        if ($optional->delete()) {
            return redirect()->route('optionals.index')->with('success', "Opcional <b>$name</b> removido com sucesso!");
        }
        
        return redirect()->back()->with('error', 'Falha ao remover opcional!');
    }
}
