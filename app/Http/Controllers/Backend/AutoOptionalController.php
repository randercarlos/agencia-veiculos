<?php

namespace App\Http\Controllers;

use App\Models\AutoOptional;
use Illuminate\Http\Request;

class AutoOptionalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AutoOptional  $autoOptional
     * @return \Illuminate\Http\Response
     */
    public function show(AutoOptional $autoOptional)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AutoOptional  $autoOptional
     * @return \Illuminate\Http\Response
     */
    public function edit(AutoOptional $autoOptional)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AutoOptional  $autoOptional
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AutoOptional $autoOptional)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AutoOptional  $autoOptional
     * @return \Illuminate\Http\Response
     */
    public function destroy(AutoOptional $autoOptional)
    {
        //
    }
}
