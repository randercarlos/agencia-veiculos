<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\Upload;
use App\Http\Controllers\Controller;
use App\Models\Testemonial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class TestemonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testemonials = Testemonial::orderBy('created_at', 'DESC')->paginate(config('constants.NUM_RECORDS_PER_PAGE'));
        
        return view('backend.testemonial.index', compact('testemonials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.testemonial.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Testemonial $testemonial)
    {
        $request->validate([
            'name' => 'required|min:5|max:100',
            'description' => 'required|min:10|max:1000',
            'photo' => 'mimes:jpeg,bmp,png|dimensions:max_width=60,max_height=60',
        ]);
        
        
        $novoNomeArquivo = Upload::execute('photo', 'testemonial');
        
        // se o resultado retornar false, é porque houve algum erro ao se fazer o upload
        if ($novoNomeArquivo === false) {
            return redirect()->back()->with('error', 'Falha ao fazer upload da foto!')->withInput();
        }
        
        // recupera os dados da requisição e adiciona o nome gerado no upload da imagem ao atributo photo
        $dadosRequisicao = $request->all();
        $dadosRequisicao['photo'] = $novoNomeArquivo;
        
        if ($testemonial->create($dadosRequisicao))
        {
            return redirect()->route('testemonials.index')->with('success', 'Depoimento criado com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao criar depoimento!')->withInput();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Testemonial  $testemonial
     * @return \Illuminate\Http\Response
     */
    public function edit(Testemonial $testemonial)
    {
        return view('backend.testemonial.form', compact('testemonial'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Testemonial  $testemonial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Testemonial $testemonial)
    {
        $request->validate([
            'name' => 'required|min:5|max:100',
            'description' => 'required|min:10|max:1000',
            'photo' => 'mimes:jpeg,bmp,png|dimensions:max_width=60,max_height=60',
        ]);
        
        
        // recupera o caminho completo da imagem antiga salva no BD para exclusão
        $pathOldPhoto = public_path() . '/uploads/testemonial/' . $testemonial->photo;
        
        // executa o upload e retorna o resultado. Se retorna o nome do arquivo, é porque o upload foi bem-sucedido
        $novoNomeArquivo = Upload::execute('photo', 'testemonial', $pathOldPhoto);
        
        // se o resultado retornar false, é porque houve algum erro ao se fazer o upload
        if ($novoNomeArquivo === false) {
            return redirect()->back()->with('error', 'Falha ao fazer upload da foto!')->withInput();
        }
        
        // recupera os dados da requisição e adiciona o nome gerado no upload da imagem ao atributo photo
        $dadosRequisicao = $request->all();
        $dadosRequisicao['photo'] = $novoNomeArquivo;
        
        if ($testemonial->update($dadosRequisicao))
        {
            return redirect()->route('testemonials.index')->with('success', 'Depoimento atualizado com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao atualizar depoimento!')->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Testemonial  $testemonial
     * @return \Illuminate\Http\Response
     */
    public function destroy(Testemonial $testemonial)
    {
        $pathOldPhoto = public_path() . '/uploads/testemonial/' . $testemonial->photo;
        if ($testemonial->delete()) {
            
            if (File::exists($pathOldPhoto)) {
                File::delete($pathOldPhoto);
            }
            
            return redirect()->route('testemonial.index')->with('success', 'Depoimento removido com sucesso!');
        }
        
        return redirect()->back()->with('error', 'Falha ao remover depoimento!')->withInput();
    }
}
