<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as EloquentModel;

class Version extends EloquentModel
{
    protected $fillable = ['name', 'model_id'];
    
    public function model()
    {
        return $this->belongsTo(Model::class);
    }
    
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = mb_strtoupper($value,'UTF-8');
    }
    
    public function getNameAttribute($value)
    {
        return mb_strtoupper($value,'UTF-8');
    }
    
    /**
     * função que retorna os modelos de automóveis agrupada pela  marca relacionada. 
     * Está formatada para ser usada numa lista dropdown.   
     *
     * @return array Lista de arrays contendo o nome do modelo e o id agrupado pela marca.
     */
    public function getModelsWithBrand()
    {
        $brands = Brand::with('models')->orderby('name')->get();
        
        $data = [];
        foreach($brands as $brand)
        {
            // é necessários recuperar os models através de Dynamic Properties 
            // em vez de Relationship Methods para evitar o problema do N + 1 queries
            $models = $brand->models->pluck('name', 'id')->toArray();
            
            // ordena os models
            asort($models);
            $data[$brand->name] = $models;
        }
        
        return $data;
    }
}
