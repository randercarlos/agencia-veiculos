<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{
    protected $fillable = ['name', 'phone', 'phone2', 'phone3', 'address', 'business_hours', 'title', 'description',
        'seo_description', 'seo_keys', 'photo'];
}
