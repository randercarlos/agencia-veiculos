<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as EloquentModel;

class Model extends EloquentModel
{
    protected $fillable = ['name', 'brand_id'];
    
    // recupera a marca associada ao modelo do automóvel
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }
    
    // recupera todas as versões do modelo
    public function versions()
    {
        return $this->hasMany(Version::class);
    }
    
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = mb_strtoupper($value,'UTF-8');
    }
    
    public function getNameAttribute($value)
    {
        return mb_strtoupper($value,'UTF-8');
    }
}
