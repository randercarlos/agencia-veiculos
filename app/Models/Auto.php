<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use App\Models\Model;

class Auto extends EloquentModel
{
    protected $fillable = ['plate', 'plate_num', 'obs', 'photo', 'year', 'price', 'is_state', 'is_negotiable',
        'km', 'ports', 'is_featured', 'url', 'is_visitable', 'is_top', 'is_offer', 'seo_description', 'seo_keys',
        'version_id', 'brand_id', 'model_id', 'color_id', 'gearbox_id', 'fuel_id', 'documentation_id', 'need_id',
        'bodywork_id'];
    protected $casts = [
        'is_featured' => 'boolean',
        'is_negotiable' => 'boolean',
        'is_visitable' => 'boolean',
        'is_top' => 'boolean',
        'is_offer' => 'boolean',
        'is_state' => 'boolean'
    ];

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function model()
    {
        return $this->belongsTo(Model::class);
    }

    public function version()
    {
        return $this->belongsTo(Version::class);
    }

    public function color()
    {
        return $this->belongsTo(Color::class);
    }

    public function gearbox()
    {
        return $this->belongsTo(Gearbox::class);
    }

    public function fuel()
    {
        return $this->belongsTo(Fuel::class);
    }

    public function documentation()
    {
        return $this->belongsTo(Documentation::class);
    }

    public function need()
    {
        return $this->belongsTo(Need::class);
    }
/*
    public function setKmAttribute($value)
    {
        $this->attributes['km'] = intval(str_replace('.', '', $value));
    }

    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = floatval(str_replace('R$ ', '', str_replace('.', '', str_replace(',', '.', $value))));
    }
*/
    public static function validationRules()
    {
        return [
            'plate_num' => 'required|alpha_dash|size:8',
            'plate' => 'required|max:8',
            'obs' => 'required|max:500',
            'photo',
            'year' => 'required|integer',
            'price' => 'required|numeric|max:999999',
            'is_state' => 'required|boolean',
            'is_negotiable' => 'nullable',
            'is_visitable' => 'nullable',
            'is_featured' => 'required|boolean',
            'is_top' => 'nullable',
            'is_offer' => 'required|boolean',
            'km' => 'required|integer',
            'ports' => 'required|integer',
            'url' => 'alpha_dash|max:100',
            'seo_description' => 'alpha_dash|max:100',
            'seo_keys' => 'alpha_dash|max:100',
            'version_id' => 'required|integer|exists:versions,id',
            'brand_id' => 'required|integer|exists:brands,id',
            'model_id' => 'required|integer|exists:models,id',
            'color_id' => 'required|integer|exists:colors,id',
            'gearbox_id' => 'required|integer|exists:gearboxes,id',
            'fuel_id' => 'required|integer|exists:fuels,id',
            'documentation_id' => 'required|integer|exists:documentations,id',
            'need_id' => 'required|integer|exists:needs,id',
            'bodywork_id' => 'required|integer|exists:bodyworks,id'
        ];
    }
}
