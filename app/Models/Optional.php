<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Optional extends Model
{
    protected $fillable = ['name', 'group_id'];
    
    
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = mb_strtoupper($value, 'UTF-8');
    }
    
    public function getNameAttribute($value)
    {
        return mb_strtoupper($value, 'UTF-8');
    }
    
    public function group()
    {
        return $this->belongsTo(Group::class);
    }
}
