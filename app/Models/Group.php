<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = ['name'];
    
    
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = mb_strtoupper($value, 'UTF-8');
    }
    
    public function getNameAttribute($value)
    {
        return mb_strtoupper($value, 'UTF-8');
    }
}
