<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as EloquentModel;

class Brand extends EloquentModel
{
    protected $fillable = ['name', 'url', 'photo'];
    
    
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = mb_strtoupper($value,'UTF-8');
    }
    
    public function getNameAttribute($value)
    {
        return mb_strtoupper($value,'UTF-8');
    }
    
    public function models()
    {
        return $this->hasMany(Model::class);
    }
}
