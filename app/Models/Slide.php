<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    protected $fillable = ['name', 'photo', 'link', 'new', 'local'];
    protected $casts = ['new' => 'boolean'];
    
    public function getNewAttribute($value)
    {
        return $value == true ? 'Sim' : 'Não';
    }
}
