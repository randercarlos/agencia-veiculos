<?php

namespace App\Services;

use App\Models\Auto;
use App\Models\Brand;
use App\Models\Color;
use App\Models\Fuel;
use App\Models\Model;
use App\Models\Gearbox;
use App\Models\Bodywork;
use App\Models\Documentation;
use App\Models\Need;
use App\Models\Group;
use App\Models\Optional;
use App\Models\Version;
use Illuminate\Http\Request;
use App\Http\Requests\AutoFormRequest;

class AutoService extends GenericService
{
    public static function findAll()
    {
        //dd($autos);
        /* $autos = Auto::with([
         'brand',
         'model',
         'version',
         'color',
         'gearbox',
         'fuel',
         'documentation',
         'need'
         ])->paginate(3); */
        
        return Auto::join('brands', 'autos.brand_id', '=', 'brands.id')
            ->join('models', 'models.id', '=', 'autos.model_id')
            ->join('versions', 'versions.id', '=', 'autos.version_id')
            ->select('autos.id as auto_id', 'models.name as model_name', 'versions.name as version_name',
                'brands.name as brand_name', 'autos.photo', 'autos.is_featured')
            ->paginate(config('constants.NUM_RECORDS_PER_PAGE'));
    }
    
    public static function find(Request $request)
    {
        
    }

    public static function findById(int $id)
    {}

    public static function save(Request $request, int $id = NULL )
    {
    }

    public static function delete(int $id)
    {}
    
    
    public static function getBrands()
    {
        return Brand::orderby('name')->pluck('name', 'id');
    }
    
    public static function getModels()
    {
        return Model::orderby('name')->pluck('name', 'id');
    }
    
    public static function getYears()
    {
        return range(date('Y'), 1950);
    }
    
    public static function getGearboxes()
    {
        return Gearbox::orderby('name')->pluck('name', 'id');
    }

    public static function getColors()
    {
        return Color::orderby('name')->pluck('name', 'id');
    }
    
    public static function getFuels()
    {
        return Fuel::orderby('name')->pluck('name', 'id');
    }
    
    public static function getBodyworks()
    {
        return Bodywork::orderby('name')->pluck('name', 'id');
    }
    
    public static function getDocumentations()
    {
        return Documentation::orderby('name')->pluck('name', 'id');
    }
    
    public static function getNeeds()
    {
        return Need::orderby('name')->pluck('name', 'id');
    }
    
    public static function getPlates()
    {
        return range(0, 9);
    }
    
    public static function getPorts()
    {
        return [0, 2, 3, 4];
    }
    
    public static function getNegotiableTypes()
    {
        return ['Aceita Troca', 'Aceita Oferta', 'Aceita Financiamento', 'Aceita Leasing', 
        'Aceita Parcelamento no Cartão de Crédito em até 12x'];
    }
    
    public static function getOptionals()
    {
        return Optional::orderby('name')->get();
    }
    
    public static function getOptionalsGroup()
    {
        return Group::orderby('name')->get();
    }
    
    public static function getVersions()
    {
        return Version::orderby('name')->pluck('name', 'id');
    }
}