<?php

namespace App\Services;

use Illuminate\Http\Request;

abstract class GenericService
{    
    // Métodos abstratos. Classes que implementem essa classe terão que ter esses métodos
    abstract public static function findAll();
    abstract public static function find(Request $request);
    abstract public static function findById(int $id);
    abstract public static function save(Request $request, int $id);
    abstract public static function delete(int $id);
}