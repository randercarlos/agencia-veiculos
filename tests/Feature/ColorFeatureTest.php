<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Color;

class ColorFeatureTest extends TestCase
{
    use RefreshDatabase;
    
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_it_can_list_colors()
    {
        $colors = [];
        for ($i = 1; $i <= 5; $i++) {
            $colors[] = Color::create(['name' => "Color {$i}"]);
        }
        
        $response = $this->get('/admin/colors');
        
        foreach($colors as $color) {
            $response->assertSee($color->name);
            
            $this->assertDatabaseHas('colors', [
                'name' => $color->name
            ]);
        }
            
    }
    
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_it_can_create_a_color()
    {
        $response = $this->from(route('colors.store'))->post(route('colors.store'), [
            'name' => 'dark sea green'
        ]);
        
        $response
            ->assertRedirect(route('colors.index'))
            ->assertSessionHasNoErrors()
            ->assertSessionHas('success');
        
            
        $this->assertDatabaseHas('colors', [
            'name' => mb_strtoupper('dark sea green')
        ]);
        
    }
    
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_it_can_create_a_color_without_required_fields()
    {
        $response = $this->from(route('colors.store'))->post(route('colors.store'), [
            'name' => null
        ]);
        
        $response
            ->assertRedirect()
            ->assertSessionHasErrors(['name']);
            
        $this->assertEmpty(Color::get()->toArray());
    }
    
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_it_can_edit_a_color()
    {
        $color = Color::create(['name' => 'Silver']);
        
        $response = $this->get(route('colors.edit', $color->id));
        
        $response
            ->assertOk()
            ->assertSee(mb_strtoupper('Silver'));
    }
    
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_it_can_update_a_color()
    {
        $color = Color::create(['name' => 'Silver']);
        
        $response = $this
            ->from(route('colors.update', $color->id))
            ->put(route('colors.update', $color->id), [
                'name' => 'Strong Silver 2'
            ]);
        
            
        $response
            ->assertRedirect(route('colors.index'))
            ->assertSessionHasNoErrors()
            ->assertSessionHas('success');
        
        $this->assertDatabaseHas('colors', [
            'name' => mb_strtoupper('Strong Silver 2')
        ]);
        
    }
    
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_it_can_update_a_color_with_nonexistent_id()
    {
        $response = $this
            ->from(route('colors.update', 1000))
            ->put(route('colors.update', 1000), [
                'name' => 'Testando'
            ]);
        
        $response->assertNotFound();
    }
   
    
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_it_can_update_a_color_without_required_fields()
    {
        $color = Color::create(['name' => 'Silver']);
        
        $response = $this
        ->from(route('colors.update', $color->id))
        ->put(route('colors.update', $color->id), [
            'name' => null
        ]);
        
        $response
            ->assertRedirect()
            ->assertSessionHasErrors(['name']);
        
        $this->assertDatabaseHas('colors', [
            'name' => mb_strtoupper($color->name)   // verifies that the name has not been changed
        ]);
        
    }
    
    
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_it_can_delete_a_color()
    {
        $color = Color::create(['name' => 'Gold']);
        
        $response = $this
            ->from(route('colors.destroy', $color->id))
            ->delete(route('colors.destroy', $color->id));
        
        $response
            ->assertRedirect(route('colors.index'))
            ->assertSessionHas('success')
            ->assertDontSee(mb_strtoupper($color->name));
        
        $this->assertDatabaseMissing('colors', [
            'name' => mb_strtoupper($color->name)   // verifies that the record has been destroyed in DB
        ]);
        
    }
    
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_it_can_delete_a_color_with_nonexistent_id()
    {
        $response = $this
            ->from(route('colors.destroy', 1000))
            ->delete(route('colors.destroy', 1000));
        
        $response->assertNotFound();
    }
}
