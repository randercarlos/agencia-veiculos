<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminAreaTest extends TestCase
{
    use RefreshDatabase;
    
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_admin_area_links_work()
    {
        $response = $this->get('/admin/autos');
        $response->assertOk();
        
        $response = $this->get('/admin/autos/create');
        $response->assertOk();
        
        $response = $this->get('/admin/brands');
        $response->assertOk();
        
        $response = $this->get('/admin/brands/create');
        $response->assertOk();
        
        $response = $this->get('/admin/models');
        $response->assertOk();
        
        $response = $this->get('/admin/models/create');
        $response->assertOk();
        
        $response = $this->get('/admin/versions');
        $response->assertOk();
        
        $response = $this->get('/admin/versions/create');
        $response->assertOk();
        
        $response = $this->get('/admin/groups');
        $response->assertOk();
        
        $response = $this->get('/admin/groups/create');
        $response->assertOk();
        
        $response = $this->get('/admin/optionals');
        $response->assertOk();
        
        $response = $this->get('/admin/optionals/create');
        $response->assertOk();
        
        $response = $this->get('/admin/gearboxes');
        $response->assertOk();
        
        $response = $this->get('/admin/gearboxes/create');
        $response->assertOk();
        
        $response = $this->get('/admin/colors');
        $response->assertOk();
        
        $response = $this->get('/admin/colors/create');
        $response->assertOk();
        
        $response = $this->get('/admin/slides');
        $response->assertOk();
        
        $response = $this->get('/admin/slides/create');
        $response->assertOk();
        
        $response = $this->get('/admin/partners');
        $response->assertOk();
        
        $response = $this->get('/admin/partners/create');
        $response->assertOk();
        
        $response = $this->get('/admin/services');
        $response->assertOk();
        
        $response = $this->get('/admin/services/create');
        $response->assertOk();
        
        $response = $this->get('/admin/socials');
        $response->assertOk();
        
        $response = $this->get('/admin/testemonials');
        $response->assertOk();
        
        $response = $this->get('/admin/testemonials/create');
        $response->assertOk();
        
        $response = $this->get('/admin/users');
        $response->assertOk();
        
        $response = $this->get('/admin/agencia');
        $response->assertOk();
        
        $response = $this->get('/admin/smtp');
        $response->assertOk();
    }
}
